package fr.projet.solitaire.metier.pieces;

/**
 * Classe de la pièce "Reine"
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Reine extends Piece
{
    /**
     * Accepte les déplacements horizontaux, verticaux et
     * en diagonale sans limite de portée.
     *  .. .. ..
     *    \| /
     *    -O-
     *   / |\
     * .. .. ..
     * @param rx Position x relative à la pièce courante.
     * @param ry Position y relative à la pièce courante.
     * @return true si la reine accepte le déplacement
     *         selon la règle ci-dessus, sinon faux.
     */
    @Override
    public boolean estDeplacable( int rx, int ry )
    {
        rx = Math.abs(rx);
        ry = Math.abs(ry);

        return rx == 0 ^ ry == 0 || rx == ry;
    }

    /**
     * Retourne si la pièces possède un obstacle sur sa route
     * @param pieces
     * @param ox
     * @param oy
     * @param x
     * @param y
     * @return
     */
    @Override
    public boolean aObstacle(Piece[][] pieces, int ox, int oy, int x, int y)
    {
        int deltaX;
        int deltaY;

        try { deltaX = (x-ox)/Math.abs(x-ox); } catch (ArithmeticException e) { deltaX = 0; }
        try { deltaY = (y-oy)/Math.abs(y-oy); } catch (ArithmeticException e) { deltaY = 0; }

        int posX = ox + deltaX;
        int posY = oy + deltaY;



        while( Math.abs(deltaX) == Math.abs(deltaY) && (posX != x && posY != y)  ||
               Math.abs(deltaX) != Math.abs(deltaY) && (posX != x || posY != y)     )
        {
            if(pieces[posY][posX] != null)
                return true;

            posX += deltaX;
            posY += deltaY;
        }


        return false;
    }
}
