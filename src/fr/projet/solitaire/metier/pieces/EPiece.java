package fr.projet.solitaire.metier.pieces;

/**
 * Enumeration des différentes pièces des échecs.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public enum EPiece
{
    CAVALIER('C', Cavalier.class),
    FOU     ('F', Fou.class     ),
    PION    ('P', Pion.class    ),
    REINE   ('Q', Reine.class   ),
    ROI     ('K', Roi.class     ),
    TOUR    ('T', Tour.class    ),
    ;

    private char                   symbole;
    private Class<? extends Piece> pieceClass;

    /**
     * Constructeur d'un type de pièce.
     *
     * @param symbole    Représentation textuelle de la pièce.
     * @param pieceClass Classe de la pièce.
     */
    EPiece(char symbole, Class<? extends Piece> pieceClass)
    {
        this.symbole    = symbole;
        this.pieceClass = pieceClass;
    }

    /**
     * Créé une pièce à partir d'un type.
     *
     * @return Une pièce lié au type donné.
     */
    public Piece creerPiece()
    {
        try
        {
            return this.pieceClass.getConstructor().newInstance();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Transforme un caractère en pièce.
     *
     * @param symbole Symbole à transformer.
     * @return        Une pièce ou null si aucune
     *                pièce n'a ce symbole.
     */
    public static Piece fromChar(char symbole)
    {
        for(EPiece ePiece : EPiece.values())
            if(ePiece.symbole == symbole)
                return ePiece.creerPiece();

        return null;
    }


}
