package fr.projet.solitaire.metier.pieces;

import javax.swing.text.Position;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe de la pièce "Roi"
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Roi extends Piece
{
    /**
     * Accepte tous les déplacements dans la limite
     * d'une case. (vertical, horizontal, diagonal)
     *
     *    \| /
     *    -O-
     *   / |\
     *
     * @param rx Position x relative à la pièce courante.
     * @param ry Position y relative à la pièce courante.
     * @return true si le roi accepte le déplacement
     *         selon la règle ci-dessus, sinon faux.
     */
    @Override
    public boolean estDeplacable( int rx, int ry )
    {
        rx = Math.abs(rx);
        ry = Math.abs(ry);

        return  rx <= 1 && ry <= 1 && rx + ry != 0;
    }

    /**
     * Cette pièce ne peut pas être prise.
     *
     * @return false.
     */
    @Override
    public boolean estPrenable()
    {
        return false;
    }

}
