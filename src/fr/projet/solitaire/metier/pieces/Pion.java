package fr.projet.solitaire.metier.pieces;

import fr.projet.solitaire.metier.Position;

import java.util.HashSet;
import java.util.Set;

/**
 * Classe de la pièce "Pion"
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Pion extends Piece
{
    /**
     * N'accepte que des déplacements vers le haut en
     * diagonal.
     *
     *    \  /
     *     O
     *
     * @param rx Position x relative à la pièce courante.
     * @param ry Position y relative à la pièce courante.
     * @return true si le pion accepte le déplacement
     *         selon la règle ci-dessus, sinon faux.
     */
    @Override
    public boolean estDeplacable(int rx, int ry)
    {
        return ry == -1 && Math.abs(rx) == 1;
    }

    /**
     * Retourne les déplacements possibles.
     * @param pieces tablier
     * @param ox position d'origine en x
     * @param oy position d'origine en y
     * @return
     */
    @Override
    public Set<Position> getDeplacementPossible(Piece[][] pieces, int ox, int oy)
    {
        Set<Position> retour = new HashSet<Position>();

        if(positionValide(ox - 1, oy - 1, pieces.length))
            retour.add(new Position(ox - 1, oy - 1));
        if(positionValide(ox + 1, oy - 1, pieces.length ))
            retour.add(new Position(ox + 1, oy - 1));

        return retour;
    }
}
