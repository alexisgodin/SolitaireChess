package fr.projet.solitaire.metier.pieces;

import fr.projet.solitaire.metier.Position;

import java.util.HashSet;
import java.util.Set;

/**
 * Classe abstraite qui represente une piece du jeu.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public abstract class Piece
{
    /**
     * Constructeur de la pièce.
     */
    public Piece() {}

    /**
     * Détermine si un pion peut être pris ou non.
     *
     * @return vrai si la pièce peut être prise.
     */
    public boolean estPrenable()
    {
        return true;
    }

    /**
     * Détermine si une pièce peut se déplacer dans la direction
     * indiquée.
     *
     * @param rx Position x relative à la pièce courante.
     * @param ry Position y relative à la pièce courante.
     * @return   true si la pièce accepte le déplacement,
     *           sinon faux.
     */
    public abstract boolean estDeplacable(int rx, int ry);

    /**
     * @param pieces
     * @param ox
     * @param oy
     * @param x
     * @param y
     * @return false car est redéfini seulement dans certaines pièces
     */
    public boolean aObstacle(Piece[][] pieces, int ox, int oy, int x, int y)
    {
        return false;
    }

    /**
     * @param pieces tablier
     * @param ox position d'origine en x
     * @param oy position d'origine en y
     * @return l'ensemble des déplacements possibles
     */
    public Set<Position> getDeplacementPossible(Piece[][] pieces, int ox, int oy)
    {
        Set<Position> retour = new HashSet<Position>();

        if(!positionValide(ox, oy, pieces.length) || pieces[oy][ox] == null)
            return retour;

        for(int lig = 0; lig < pieces.length;lig++)
            for(int col = 0; col < pieces.length; col++)
            {
                if(lig == oy && col == ox)
                    continue;

                if(  pieces[oy][ox].estDeplacable(col - ox, lig - oy)   &&
                    !pieces[oy][ox].aObstacle(pieces, ox, oy, col, lig)    )
                {
                    if(pieces[lig][col] != null && !pieces[lig][col].estPrenable())
                        continue;

                    retour.add(new Position(col, lig));
                }
            }

        return retour;
    }

    /**
     *
     * @param x
     * @param y
     * @param taille
     * @return si la position passée en paramètre se trouve sur le plateau
     */
    public static boolean positionValide(int x, int y, int taille)
    {
        return !(x < 0 || x >= taille) && !(y < 0 || y >= taille);
    }
}

