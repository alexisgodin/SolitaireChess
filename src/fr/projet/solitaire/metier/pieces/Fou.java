package fr.projet.solitaire.metier.pieces;

import java.util.HashSet;
import java.util.Set;

/**
 * Classe de la pièce "Fou"
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Fou extends Piece
{
    /**
     * N'accepte que des déplacements en diagonal
     * sans limite de distance.
     *    ...  ...
     *      \  /
     *       O
     *     /  \
     *   ...  ...
     * @param rx Position x relative à la pièce courante.
     * @param ry Position y relative à la pièce courante.
     * @return true si le fou accepte le déplacement
     *         selon la règle ci-dessus sinon faux.
     */
    @Override
    public boolean estDeplacable(int rx, int ry)
    {
        return Math.abs(rx) == Math.abs(ry);
    }

    /**
     * Retourne si la pièces possède un obstacle sur sa route
     * @param pieces
     * @param ox
     * @param oy
     * @param x
     * @param y
     * @return
     */
    @Override
    public boolean aObstacle(Piece[][] pieces, int ox, int oy, int x, int y)
    {
        int deltaX;
        int deltaY;

        try { deltaX = (x-ox)/Math.abs(x-ox); } catch (ArithmeticException e) { deltaX = 0; }
        try { deltaY = (y-oy)/Math.abs(y-oy); } catch (ArithmeticException e) { deltaY = 0; }

        int posX = ox + deltaX;
        int posY = oy + deltaY;



        while(posX != x && posY != y)
        {
            if(pieces[posY][posX] != null)
                return true;

            posX += deltaX;
            posY += deltaY;
        }


        return false;
    }
}
