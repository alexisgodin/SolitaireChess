package fr.projet.solitaire.metier.pieces;

/**
 * Classe de la pièce "Cavalier"
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */

public class Cavalier extends Piece
{
    /**
     * N'accepte que les déplacements de 2 cases sur l'axe
     * x ou l'axe y mais pas les 2, et pour l'axe contraire,
     * un déplacement d'une case.
     *
     *      -|-
     *     | | |
     *     --O--
     *     | | |
     *      -|-
     *
     * @param rx Position x relative à la pièce courante.
     * @param ry Position y relative à la pièce courante.
     * @return true si le cavalier accepte le déplacement
     *         selon la règle ci-dessus, sinon faux.
     */
    @Override
    public boolean estDeplacable(int rx, int ry)
    {
        rx = Math.abs(rx);
        ry = Math.abs(ry);

        return rx == 2 && ry == 1 || ry == 2 && rx == 1;
    }
}
