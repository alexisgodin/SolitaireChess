package fr.projet.solitaire.metier;

import fr.projet.solitaire.metier.pieces.EPiece;
import fr.projet.solitaire.metier.pieces.Piece;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Classe qui gere un Defi.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Defi
{
    /**
     * Nombres de coups sur tout les défis.
     */
    private static int nbCoups     = 0;

    /**
     * Nom du défi
     */
    private String     nom;

    /**
     * Niveau de difficulté du défi.
     */
    private Niveau     niveau;

    /**
     * Tableau des pieces qui permet de connaitre l'état du tablier.
     */
    private Piece[][]  pieces;

    /**
     * Si le défi est débloqué
     */
    private boolean    estDebloque;

    /**
     * Nombre de pièces dans le défi.
     * Est décrémenté à chaque prise de pièces.
     * Permet de savoir si le niveau est terminé.
     */
    private int        nbPieces;

    /**
     * Pour gérer les pénalités, ect. .
     */
    private  int       nbCoupsDefi;

    /**
     * Permet de connaître les coups précédants.
     * Utiliser pour annuler.
     */
    private LinkedList<Piece[][]> coupsPrecedents;

    /**
     * Sauvegarde les coups à faire pour finir le défi.
     */
    private LinkedList<String> aides;
    /**
     * Indice du coup dans la sauvegarde des coups.
     */
    private int indiceAide;

    /**
     * Constructeur.
     * @param nom du défi
     * @param niveau de difficulté du défi
     */
    private Defi(String nom, Niveau niveau)
    {
        this.nom         = nom;
        this.niveau      = niveau;
        this.estDebloque = false;
        this.nbPieces    = 0;

        this.coupsPrecedents = new LinkedList< Piece[][] >();

        this.nbCoupsDefi = 0;
    }

    /**
     * Constructeur sans paramètre.
     */
    public Defi()
    {
        this("", null);
    }

    /**
     * Getter.
     * @return le nombre de pièces
     */
    public int getNbPieces()
    {
        return this.nbPieces;
    }


    /**
     * @return le nom du défi
     */
    public String getNom()
    {
        return this.nom;
    }

    /**
     * @return le niveau de difficulté
     */
    public Niveau getNiveau()
    {
        return this.niveau;
    }

    /**
     * @return le tablier
     */
    public Piece[][] getPieces()
    {
        return this.pieces;
    }

    /**
     * @return le nombre de coups au total
     */
    public static int getNbCoups()
    {
        return Defi.nbCoups;
    }

    /**
     * @return le nombre de coups effectués pour ce défi
     */
    public int getNbCoupsDefi()
    {
        return this.nbCoupsDefi;
    }

    /**
     * @return si le défi est débloqué
     */
    public boolean estDebloque()
    {
        return this.estDebloque;
    }

    /**
     * Setter.
     * @param nbCoupDef
     */
    public void setNbCoupsDefi(int nbCoupDef)
    {
        this.nbCoupsDefi = nbCoupDef;
    }

    /**
     * Setter.
     * @param nom
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * Setter.
     * @param estDebloque
     */
    public void setEstDebloque(boolean estDebloque)
    {
        this.estDebloque = estDebloque;
    }

    /**
     * Ajoute la variation aux nombres de coups.
     * @param nb variation
     */
    public static void setNbCoups(int nb) {
        Defi.nbCoups += nb;
    }

    /**
     * Setter.
     * @param niveau à changer
     */
    public void setNiveau(Niveau niveau)
    {
        this.niveau = niveau;
    }

    /**
     * @return si le défi possède une aide
     */
    public boolean defiPossedeAide()
    {
        if(aides == null)
            Defi.Loader.chargerAides(this);
        return this.aides.size() != 0 && !this.aides.get(0).equals("");
    }

    /**
     * @return le départ et l'arrivée du prochain coup à faire
     */
    public Position[] getAide()
    {
        if(this.aides == null)
            Defi.Loader.chargerAides(this);
        if(this.aides.size() == 0)
            return null;

        return coupsFromString(this.aides.get(this.indiceAide));
    }

    /**
     * Incrémente l'indice de l'aide.
     */
    public void incrementerIndiceAide()
    {
        if(this.aides == null)
            Defi.Loader.chargerAides(this);
        if(this.indiceAide < this.aides.size() - 1)
            this.indiceAide++;
    }

    /**
     * Recupere un coup à partir d'une chaîne de caractère.
     *
     * @param s coup sous forme chainée
     * @return l'arrivée et le départ du coup passé en paramètre
     */
    private Position[] coupsFromString(String s)
    {
        String[] tmp = s.split("-");

        return new Position[]{
          new Position( (int)tmp[0].charAt(0) - (int)'A',
                        (int)tmp[0].charAt(1) - (int)'1'  ),
          new Position( (int)tmp[1].charAt(0) - (int)'A',
                        (int)tmp[1].charAt(1) - (int)'1'  ),
        };
    }

    /**
     * Déplace le pion si il se trouve sur l'origine et que le déplacement est valide.
     *
     * @param ox Pion à déplacer.
     * @param oy Pion à déplacer.
     * @param x  Position x ciblée.
     * @param y  Position y ciblée.
     * @return la case d'arrivée (une Piece)
     */
    public Piece deplacer(int ox, int oy, int x, int y)
    {
        if(x == -1 || y ==-1)
            return null;

        if (ox == x && oy == y)
            return null;

        if (!estPiece(ox, oy) || !estPiece(x, y))
            return null;

        Piece courante    = this.pieces[oy][ox];
        Piece cible       = null;

        if(courante == null)
            return  null;

        if (courante.estDeplacable(x - ox, y - oy) && !courante.aObstacle(this.pieces, ox, oy, x, y))
        {
            if(this.pieces[y][x] != null && this.pieces[y][x].estPrenable())
            {
                this.coupsPrecedents.addLast( cloneTab(this.pieces) );

                cible               = this.pieces[y][x];
                this.pieces[y][x]   = courante;
                this.pieces[oy][ox] = null;

                this.nbPieces--;
            }
        }

        Defi.nbCoups++;
        this.nbCoupsDefi++;

        return cible;
    }

    /**
     * Retourne une copie d'un tableau de pièce.
     * @param pieces tableau de pièce à copier
     * @return la copie
     */
    private static Piece[][] cloneTab(Piece[][] pieces)
    {
        Piece[][] copie = new Piece[pieces.length][];

        for (int i = 0; i < pieces.length; i++)
        {
            copie[i] = new Piece[pieces[i].length];

            for (int j = 0; j < pieces.length; j++)
                copie[i][j] = pieces[i][j];
        }

        return copie;

    }

    /**
     * Annule la dernière action effectuer.
     * @return si l'annulation s'est effectuée
     */
    public boolean annulerAction()
    {
        if (this.coupsPrecedents != null && this.coupsPrecedents.size() > 0)
        {
            this.pieces = this.coupsPrecedents.getLast();
            this.coupsPrecedents.removeLast();

            this.nbPieces++;

            return true;
        }

        return false;
    }

    /**
     * @return si le défi est terminé
     */
    public boolean aGagne()
    {
        return this.nbPieces <= 1;
    }

    /**
     * @param x coordonnée
     * @param y coordonnée
     * @return si la position est une pièce
     */
    private boolean estPiece(int x, int y) {
        return  0 <= y && y < this.pieces.length &&
                0 <= x && x < this.pieces[y].length &&
                this.pieces[y][x] != null;
    }

    /**
     * Enumération interne qui gère les niveaus de difficultés.
     */
    public enum Niveau
    {
        DEBUTANT     (new Color(174, 224, 167)),
        INTERMEDIAIRE(new Color(198, 198, 255)),
        AVANCE       (new Color(215, 174, 255)),
        EXPERT       (new Color(232, 142, 130)),
        ;

        /**
         * Couleur correspondante au niveau de difficulté.
         * Celle utilisée pour le quadrillage.
         */
        Color couleur;

        /**
         * Constructeur.
         * @param couleur du niveau de difficulté
         */
        Niveau(Color couleur)
        {
            this.couleur = couleur;
        }

        /**
         * @return la couleur du niveau
         */
        public Color getCouleur()
        {
            return this.couleur;
        }

        /**
         * @return le nom du iveau de difficulté
         */
        public String getNom()
        {
            return this.name().toLowerCase();
        }
    }

    /**
     * Classe interne qui permet de charger les défis.
     */
    public static class Loader
    {
        /**
         * Repertoire ou se trouve les défis.
         */
        public static final String rep = "aides/";

        /**
         * Extension des fichiers où sont stockés les défis.
         */
        public static final String ext = ".txt";

        /**
         * Méthode qui charge le défi.
         * @param fichier du défi
         * @return le défi chargé
         */
        public static Defi charger(File fichier)
        {
            Defi defi = null;
            try
            {
                Scanner scLigne;
                int     nLigne = 0;
                String  ligne;

                scLigne = new Scanner(new FileReader(fichier));
                while(scLigne.hasNextLine())
                {
                    nLigne++;
                    scLigne.nextLine();
                }


                defi = new Defi();
                defi.pieces = new Piece[nLigne][nLigne];

                scLigne = new Scanner(new FileReader(fichier));
                int cptLigne = 0;
                while(scLigne.hasNextLine())
                {
                    ligne = scLigne.nextLine();
                    for(int cptCol = 0; cptCol < ligne.length(); cptCol ++)
                    {
                        Piece piece = EPiece.fromChar(ligne.charAt(cptCol));

                        if (piece != null)
                            defi.nbPieces++;

                        defi.pieces[cptLigne][cptCol] = piece;
                    }
                    cptLigne++;
                }
            } catch (Exception ignored) {}

            if(defi != null)
                defi.setNbCoupsDefi(0);

            return defi;
        }

        /**
         * Méthode qui charge l'aide pour un défi donné.
         * @param defi dont on veut l'aide
         */
        public static void chargerAides(Defi defi)
        {
            defi.aides = new LinkedList<String>();

            try
            {
                Scanner scLigne = new Scanner(
                        new File(Loader.rep + defi.getNiveau().name().toLowerCase() +
                                 "/" + defi.getNom().replaceAll(" ","")      + Loader.ext  )
                );

                while(scLigne.hasNextLine())
                {
                    defi.aides.add(scLigne.nextLine());
                }
            } catch (FileNotFoundException ignored) {}
        }
    }
}
