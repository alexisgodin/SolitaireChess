package fr.projet.solitaire.metier;

/**
 * Classe qui gere les positions
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Position
{
    /**
     * Coordonnées en  x et en y
     */
    private int x,y;

    /**
     * Constructeur
     * @param x coordonnée
     * @param y coordonnée
     */
    public Position(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * @return la coordonnée en x
     */
    public int getX()
    {
        return this.x;
    }

    /**
     * @return la coordonnée en y
     */
    public int getY()
    {
        return this.y;
    }

    @Override
    public boolean equals(Object obj)
    {
        return this == obj || obj instanceof Position && ((Position) obj).x == this.x && ((Position) obj).y == this.y;
    }

    /**
     * @return l'objet sous forme chainée
     */
    public String toString() { return "(" + this.x + "," + this.y + ")"; }
}
