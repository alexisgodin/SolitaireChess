package fr.projet.solitaire.metier.profil;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Fichier de sauvegarde d'un profil.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class FichierProfil
{
    /**
     * Emplacement des profils.
     */
    static final String DIR = "profils";

    private File   fichier;
    private Profil profil;

    /**
     * Constructeur du gestionnaire de fichier
     * d'un profil donné.
     *public
     * @param profil Profil lié à ce gestionnaire.
     */
    private FichierProfil(Profil profil)
    {
        this.profil  = profil;
        this.fichier = new File(this.getPath());
    }

    /**
     * Créé une instance du gestionnaire si le profil
     * donné n'est pas null.
     *
     * @param profil Profil à gérer.
     * @return       Le gestionnaire du profil ou null
     *               si le paramètre est null.
     */
    static FichierProfil getInstance(Profil profil)
    {
        if (profil == null) return null;
        return new FichierProfil(profil);
    }

    /**
     * Récupère l'emplacement relatif du fichier du profiL.
     *
     * @return L'emplacement relatif du fichier du profil.
     */
    private String getPath()
    {
        return FichierProfil.DIR + "/" + this.profil.getNom() + ".data";
    }

    /**
     * Charge le profil à partir du fichier.
     *
     * @return true si le fichier existe.
     */
    public boolean charger()
    {
        Scanner  scLigne;
        int      cpt    = 0;
        int      nLigne = 0;
        String[] entrees;
        File     fichier;
        try
        {
            fichier = new File(this.getPath());
            if (fichier.exists())
            {
                scLigne = new Scanner(fichier);
                while (scLigne.hasNextLine())
                {
                    scLigne.nextLine();
                    nLigne++;
                }
                scLigne.close();

                entrees = new String[nLigne];
                scLigne = new Scanner(new FileReader(this.getPath()));
                while (scLigne.hasNextLine())
                    entrees[cpt++] = scLigne.nextLine();

                this.profil.convertFromString(entrees);
                return true;
            }
            else
                System.out.println("Le fichier " + this.getPath() + " n'existe pas.");

        }
        catch (Exception ex)
        {
            System.err.println("Une erreur est survenue lors du chargement du profil " +
                               this.profil.getNom() + ".");
            ex.printStackTrace();
        }

        return false;
    }

    /**
     * Enregistre le profil dans son fichier.
     *
     * @return true si enregistré, false en cas d'erreur.
     */
    public boolean enregistrer()
    {
        FileWriter fw;
        try
        {
            if (new File(FichierProfil.DIR).mkdirs())
                System.out.println("Dossier des profils créé.");

            fw = new FileWriter(this.fichier, false);
            fw.write(this.profil.convertToString());
            fw.close();
            return true;
        }
        catch (IOException ex)
        {
            System.err.println("Une erreur est survenue lors de l'enregistrement du profil " +
                               this.profil.getNom() + ".");
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Sauvegarde le profil.
     */
    public void sauvegarder()
    {
        if (this.fichier != null)
            this.fichier.delete();

        this.fichier = new File( this.getPath() );
        this.enregistrer();
    }

    /**
     * Supprime le fichier du profil.
     *
     * @return true si le fichier a bin été supprimé.
     */
    public boolean supprimer()
    {
        return this.fichier.delete();
    }
}
