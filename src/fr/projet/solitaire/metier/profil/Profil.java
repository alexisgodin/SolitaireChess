package fr.projet.solitaire.metier.profil;

import java.util.HashSet;
import java.util.Set;

/**
 * Profil d'un joueur.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Profil
{
    private String nom;
    private int defiCourant;
    private int score;
    private Set< String > defisFait;
    private FichierProfil fichierProfil;

    /**
     * Initialise le joueur.
     * Ne peut que prendre un nom non null et non vide.
     *
     * @param nom Nom du joueur.
     */
    private Profil( String nom )
    {
        this.nom = nom;
        this.defisFait = new HashSet< String >();
        this.fichierProfil = FichierProfil.getInstance( this );
    }

    /**
     * Usine à profil, vérifie si le nom donné est
     * non null et non vide.
     *
     * @param nom Nom du profil.
     * @return Un profil si le nom est valide sinon null.
     */
    public static Profil creer( String nom )
    {
        if ( nom == null || nom.isEmpty() ) return null;
        return new Profil( nom );
    }

    /**
     * Récupère le nom du profil.
     *
     * @return Nom du profil.
     */
    public String getNom()
    {
        return this.nom;
    }

    /**
     * Définit le nom du profil.
     *
     * @param nom le nouveau nom
     */
    public void setNom( String nom )
    {
        this.nom = nom;
        this.getFichierProfil().sauvegarder();
    }

    /**
     * Récupère le gestionnaire du fichier du profil.
     *
     * @return Le gestionnaire du fichier du profil.
     */
    public FichierProfil getFichierProfil()
    {
        return this.fichierProfil;
    }

    /**
     * Récupère l'indice du niveau courant.
     * Cet indice n'est pas lié avec l'ensemble
     * des niveaux fait.
     *
     * @return L'indice du niveau courant.
     */
    public int getDefiCourant()
    {
        return this.defiCourant;
    }

    /**
     * Définie le défi courant.
     *
     * @param defiCourant Indice du défi en cours.
     */
    public void setDefiCourant( int defiCourant )
    {
        this.defiCourant = defiCourant;
    }

    /**
     * Récupère le score du profil.
     *
     * @return Le score du profil.
     */
    public int getScore()
    {
        return this.score;
    }

    /**
     * Définie le score du joueur.
     *
     * @param score Le score à définir au profil.
     */
    public void setScore( int score )
    {
        this.score = score;
    }

    /**
     * Vérifie si un défi est déjà fait et terminé par le joueur.
     *
     * @param nomDefi Nom du défi à vérifier.
     * @return true si le défi est déjà fait et terminé.
     */
    public boolean estFait( String nomDefi )
    {
        return this.defisFait.contains( nomDefi );
    }

    /**
     * Ajoute un nom de défi aux défis terminés.
     *
     * @param nomDefi Nom du défi terminé.
     */
    public void ajouterDefiFait( String nomDefi )
    {
        this.defisFait.add( nomDefi );
    }

    /**
     * Réinitialise à 0 les stats du profil.
     */
    public void reinitialiser()
    {
        this.defisFait.clear();
        this.score = this.defiCourant = 0;
    }

    /**
     * Méthode accessible depuis FichierProfil.
     * Charge l'ensemble des attributs du profil.
     *
     * @param entrees Lignes à convertir.
     */
    void convertFromString( String[] entrees )
    {
        this.reinitialiser();

        if ( entrees.length > 0 )
            this.score = Integer.parseInt( entrees[0] );

        if ( entrees.length > 1 )
            this.defiCourant = Integer.parseInt( entrees[1] );

        if ( entrees.length > 2 )
        {
            String[] nomDefis = entrees[2].split( ";" );
            for ( String nomDefi : nomDefis )
                ajouterDefiFait( nomDefi );
        }
    }

    /**
     * Méthode accessible depuis FichierProfil.
     * Convertie les attributs sous une forme texture qui
     * peut être chargé par le profil.
     * <p>
     * Le nom n'est pas récupéré car il est utilisé pour
     * nommer le fichier.
     *
     * @return Les attributs sous forme textuelle.
     */
    String convertToString()
    {
        String res = "";
        res += this.score + "\n";
        res += this.defiCourant + "\n";

        for ( String nomDefi : this.defisFait )
            res += nomDefi + ";";

        return res;
    }

    /**
     * Récupère l'ensemble des attributs de la classe.
     *
     * @return Les attributs de la classe.
     */
    @Override
    public String toString()
    {
        return this.nom + "\n" + this.convertToString();
    }
}
