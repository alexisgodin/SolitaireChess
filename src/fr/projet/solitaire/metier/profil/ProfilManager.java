package fr.projet.solitaire.metier.profil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe ProfilManager qui gère l'ensemble des profils
 * contenus dans le dossier des profils.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class ProfilManager
{
    /**
     * Liste des profils chargés.
     */
    private List< Profil > profils;

    /**
     * Profil qui joue actuellement.
     */
    private Profil profilCourant;

    /**
     * Initialisation du ProfilManager.
     */
    public ProfilManager()
    {
        this.profils = new ArrayList< Profil >();

        this.chargerProfils();
    }

    /**
     * Charge tous les profils du dossier contenant les profils.
     */
    private void chargerProfils()
    {
        File dir = new File( FichierProfil.DIR );
        File[] fichiers = dir.listFiles();

        if ( fichiers != null )
        {
            for ( File fichier : fichiers )
            {
                String nom = fichier.getName().split( "\\." )[0];
                Profil p = Profil.creer( nom );

                if ( p != null )
                {
                    p.getFichierProfil().charger();
                    this.profils.add( p );
                }
            }
        }
    }

    /**
     * @return une liste contenant le nom de tous les profils
     */
    public String[] getListeNom()
    {
        String[] listeNom = new String[this.profils.size()];

        for ( int i = 0; i < this.profils.size(); i++ )
            listeNom[i] = this.profils.get( i ).getNom();

        return listeNom;
    }

    /**
     * Crée un nouveau profil et l'enregistre dans le dossier
     * contenant les profils.
     *
     * @param nom le nom profil
     * @return vrai si le profil a pu être créé
     */
    public boolean creerProfil( String nom )
    {
        Profil p = Profil.creer( nom );

        if ( p != null )
        {
            p.getFichierProfil().enregistrer();
            this.profils.add( p );

            return true;
        }

        return false;
    }

    /**
     * Charge un profil en fonction du nom passé
     * en paramètre.
     *
     * @param nom le nom du profil
     * @return vrai si le profil a pu être chargé
     */
    public boolean chargerProfil( String nom )
    {
        for ( Profil p : this.profils )
        {
            if ( p.getNom().equals( nom ) )
            {
                this.profilCourant = p;
                return true;
            }
        }

        return false;
    }

    /**
     * @return le profil actuellement en train de jouer
     */
    public Profil getProfilCourant()
    {
        return this.profilCourant;
    }
}
