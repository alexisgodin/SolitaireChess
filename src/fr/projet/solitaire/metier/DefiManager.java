package fr.projet.solitaire.metier;

import fr.projet.solitaire.metier.profil.Profil;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


/**
 * Classe DefiManager qui gère les différents défis du jeu.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */

public class DefiManager
{
    /**
     * Repertoire des défis
     */
    private static final String DIR = "defis/";

    /**
     * Ensemble des défis disponibles
     */
    private LinkedList<Defi> defis;

    /**
     * Défi sur lequel le manager pointe
     */
    private int              defiCourant;

    /**
     * booléen pour ne pas charger plusieurs fois l'ensemble des défis
     */
    private boolean          init;

    /**
     * Nombre de coups optimal par rapport au niveau maximal
     */
    private int nbCoupMinimum;

    /**
     * Constructeur
     */
    public DefiManager() {
        this.defis          = new LinkedList<Defi>();
        this.defiCourant = 0;

        this.chargerDefis();

        this.defis.get( this.defiCourant ).setEstDebloque( true );
        this.nbCoupMinimum = this.getDefiCourant().getNbPieces() - 1;
    }

    /**
     * Constructeur
     * @param p profil de l'utilisateur actuel
     */
    public DefiManager( Profil p )
    {
        this();

        for (Defi d : this.defis)
        {
            if(p.estFait(d.getNom()))
            {
                d.setEstDebloque(true);
                this.nbCoupMinimum += d.getNbPieces()-1;
            }
        }

        this.nbCoupMinimum -= this.getDefiCourant().getNbCoupsDefi();


        this.defiCourant = p.getDefiCourant();
        this.defis.get( this.defiCourant ).setEstDebloque( true );

        Defi.setNbCoups(p.getScore() - Defi.getNbCoups());
    }

    /**
     * @return le défi courant
     */
    public Defi getDefiCourant()
    {
        return this.defis.get(this.defiCourant);
    }

    /**
     * @return l'indice du défi courant
     */
    public int getIndiceDefiCourant()
    {
        return this.defiCourant;
    }

    /**
     * Retourne les défis en fonction de leur niveau
     * Si le niveau est null on retourne tout les défis
     * @param n niveau dont on veut les défis
     * @return les défis selectionnés
     */
    public LinkedList<Defi> getDefis(Defi.Niveau n)
    {
        if(n == null)
            return this.defis;

        LinkedList<Defi> retour = new LinkedList<Defi>();

        for(Defi defi : this.defis)
        {
            if(defi.getNiveau() == n)
                retour.add(defi);
        }

        return retour;
    }

    /**
     * @return l'ensemble des défis
     */
    public LinkedList< Defi > getDefis()
    {
        return this.defis;
    }

    /**
     * @param niveau dont on veut connaître le nombre de défis débloqués
     * @return le nombre de défis débloqués du niveau passer en paramètre
     */
    public int getNbDefisDebloquesNiveau(Defi.Niveau niveau)
    {
        int nbDefisDebloques = 0;

        for (Defi d : this.defis)
        {
            if (d.getNiveau() == niveau)
            {
                if (d.estDebloque())
                    nbDefisDebloques++;
            }
        }

        return nbDefisDebloques;
    }

    /**
     * Getter
     * @return le nombre de coups minimum
     */
    public int getNbCoupMinimum()
    {
        return this.nbCoupMinimum;
    }

    /**
     * @return si le défi courant a un défi débloqué après lui
     */
    public boolean aUnDefiSuivantDbloque()
    {
        int cpt = this.defiCourant + 1;
        while(cpt < this.defis.size())
        {
            if(this.defis.get(cpt).estDebloque())
                return true;
            cpt++;
        }

        return false;
    }

    /**
     * @return le défi du prochain niveau(celui après le courant)
     */
    public Defi getDefiDuProchianNiveau()
    {
        int indice = this.defiCourant;
        while(indice < this.defis.size())
        {
            if (this.defis.get(indice).getNiveau() != this.getDefiCourant().getNiveau())
                return this.defis.get(indice);
            indice ++;
        }

        return this.getDefiCourant();
    }

    /**
     * @param niveau
     * @return le nombre de défis d'un niveau donné
     */
    public int getNbDeDefiDe(Defi.Niveau niveau)
    {
        int cpt = 0;
        for(Defi defi : this.defis)
        {
            if(defi.getNiveau() == niveau)
                cpt ++;
        }

        return cpt;
    }

    /**
     * @param indice
     * @return le défi d'indice donné
     */
    public Defi getDefi( int indice )
    {
        return this.defis.get( indice );
    }

    /**
     * @param defi
     * @return l'indice d'un défi donné
     */
    public int getIndiceDefi(Defi defi)
    {
        if (defi != null && this.defis.contains( defi ))
            return this.defis.indexOf( defi );

        return -1;
    }

    /**
     * Setter
     * @param indice auquel on veut placer le courant
     */
    public void setDefiCourant(int indice)
    {
        this.defiCourant = indice;
        this.rechargerDefis();
    }

    /**
     * Ajoute le nombre de pièces du défi courant au nombre minimal
     */
    public void ajouterNbCoupDuDefiCourant()
    {
        this.nbCoupMinimum += this.getDefiCourant().getNbPieces() - 1;
    }

    /**
     * Le manager passe au défi suivant si possible
     * @return si on a pu passer au niveau suivant
     */
    public boolean defiSuivant()
    {
       if (this.defiCourant >= this.defis.size() - 1)
           return false;
        if(this.getDefiCourant().aGagne())
        {
            this.defis.get(++this.defiCourant).setEstDebloque(true);
            this.rechargerDefis();
            return true;
        }

        int tmp = ++this.defiCourant;
        if(this.defiCourant == this.defis.size() - 2)
            return true;

        while(tmp < this.defis.size() && !this.defis.get(tmp).estDebloque())
            tmp++;

        if(tmp == this.defis.size())
        {
            if(this.defis.get(this.defiCourant).estDebloque())
                return true;
            else
            {
                this.defiCourant --;
                return false;
            }
        }

        this.defiCourant = tmp;

        this.rechargerDefis();

        return true;
    }

    /**
     * Le manager passe au défi précédant si possible
     * @return si on a pu passer au niveau précédant
     */
    public boolean defiPrecedent()
    {
        if (this.defiCourant <= 0)
            return false;

        this.defiCourant--;

        int tmp = this.defiCourant;
        while(tmp > 0 && !this.defis.get(tmp).estDebloque())
            tmp--;

        if(tmp > 0)
            this.defiCourant = tmp;

        this.rechargerDefis();

        return true;
    }

    /**
     * Annule la dernière action effectuée
     * @return si l'action a été annulée
     */
    public boolean annulerAction()
    {
        return this.getDefiCourant().annulerAction();
    }

    /**
     * Ajoute au nombre de coups minimums le nombre de coups du défi passé en paramètre
     * @param defiADebloque
     */
    public void ajouterNbCoupDuDefi(Defi defiADebloque)
    {
        this.nbCoupMinimum += defiADebloque.getNbPieces() -1;
    }

    /**
     * Recharge le défi courant
     */
    private void rechargerDefis()
    {
        Defi defi    = this.defis.get(this.defiCourant);
        File fichier = new File("defis/"+ defi.getNiveau().getNom() +
                                "/defi" + String.format("%02d",this.defiCourant + 1) + ".txt");

        try
        {
            Defi nDefi = Defi.Loader.charger(fichier);

            nDefi.setNom(defi.getNom());
            nDefi.setNiveau(defi.getNiveau());
            nDefi.setEstDebloque(defi.estDebloque());


            this.defis.set(this.defiCourant, nDefi);
        }
        catch(Exception e){}
    }

    /**
     * Charge tout les défis
     */
    private void chargerDefis()
    {
        if (this.init)
            return;

        for (Defi.Niveau n : Defi.Niveau.values())
        {
            File       dir      = new File(DefiManager.DIR, n.name().toLowerCase());
            List<File> liste    = new ArrayList<File>();
            File[]     fichiers = dir.listFiles();

            if (fichiers != null)
            {
                Collections.addAll(liste, fichiers);
                Collections.sort(liste);

                for (File fichier : liste)
                {
                    if (fichier.getName().startsWith("defi"))
                    {
                        Defi d = Defi.Loader.charger(fichier);
                        if (d != null)
                        {
                            d.setNom("defi " + fichier.getName().split("(defi)|(.txt)")[1]);
                            d.setNiveau(n);
                            this.defis.add(d);
                        }
                    }
                }
            }
        }

        this.init = true;
    }
}
