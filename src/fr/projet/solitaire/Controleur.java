package fr.projet.solitaire;

import fr.projet.solitaire.ihm.Fenetre;
import fr.projet.solitaire.metier.Defi;
import fr.projet.solitaire.metier.DefiManager;
import fr.projet.solitaire.ihm.GestionnaireAide;
import fr.projet.solitaire.metier.pieces.Piece;
import fr.projet.solitaire.metier.profil.Profil;
import fr.projet.solitaire.metier.profil.ProfilManager;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Classe principale Controleur.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Controleur
{
    /**
     * Gestionnaire de défis.
     */
    private DefiManager manager;

    /**
     * Gestionnaire des utilisateurs.
     */
    private ProfilManager profilManager;

    /**
     * Fenêtre du logiciel.
     */
    private Fenetre     fenetre;

    /**
     * Booléen qui dit que l'aide est autorisé,
     * Si non, alors l'aide ne sera jamais proposée.
     */
    private boolean aideAutorise = true;

    /**
     * Indice de l'avertissement, utilisé pour rajouter une pénalité si le joueur essaye de réinitaialiser
     * En faisant précédant puis suivant.
     */
    private int     indiceAvert;

    /**
     * Gestionnaire d'aide
     */
    private GestionnaireAide gestionAide;

    /**
     * Constructeur
     */
    public Controleur()
    {
        this.manager = new DefiManager();
        this.profilManager = new ProfilManager();
        this.fenetre = new Fenetre(this);
        this.debloquerDefis();
        this.indiceAvert = -1;

        this.gestionAide = new GestionnaireAide(this, this.fenetre.getJeu().getPlateau());
    }

    /**
     * @return le tablier
     */
    public Piece[][] getPieces()
    {
        Defi courant = this.manager.getDefiCourant();
        return courant == null ? null : courant.getPieces();
    }

    /**
     * @return le nombre de coup minimum par rapport à tous les défis débloqués
     */
    public int getNbCoupMinimum()
    {
        return this.manager.getNbCoupMinimum();
    }

    /**
     * Méthode qui déplace la pièce si les coordonnées passées sont valides.
     * @param ox
     * @param oy
     * @param x
     * @param y
     */
    public void deplacer(int ox, int oy, int x, int y)
    {
        Defi courant = this.manager.getDefiCourant();

        if (courant != null)
        {
            Piece tmp = courant.deplacer(ox, oy, x, y);
            if(tmp != null) {
                this.fenetre.getJeu().tuerPiece(tmp);

                if (courant.aGagne()) {
                    this.fenetre.getJeu().getPlateau().update();
                    if(this.fenetre.getControleur().getIndiceDefiCourant() == 59)
                        JOptionPane.showMessageDialog(this.fenetre, "Félicitations !!! Vous avez complété le dernier défi du jeu !", "Victoire", JOptionPane.INFORMATION_MESSAGE);
                    else
                        JOptionPane.showMessageDialog(this.fenetre, "Bravo, vous avez terminé le défi", "Victoire", JOptionPane.INFORMATION_MESSAGE);
                    this.manager.defiSuivant();
                    this.debloquerDefis();
                    this.indiceAvert = -1;
                }
                else
                    this.manager.getDefiCourant().incrementerIndiceAide();

                this.gestionAide.restartTimer();

                if(this.fenetre.getJeu().getPlateau().getAide())
                {
                    this.fenetre.getJeu().getPlateau().setAide(false);
                }

                this.fenetre.getJeu().update();
            }
        }
    }

    /**
     * Méthode qui débloque les défis, pour le gestionnaire et les menus
     */
    private void debloquerDefis()
    {
        this.debloquerDefi(
                this.manager.getDefiCourant().getNiveau(),
                this.manager.getDefiCourant()
        );

        /**
         * - 1 car sinon on vérifie un niveau qui n'est pas finis
         * + 1 pour arrondir au dessus
         */
        if ( this.manager.getNbDefisDebloquesNiveau( this.manager.getDefiCourant().getNiveau() ) - 1 >=
             this.manager.getNbDeDefiDe(this.manager.getDefiCourant().getNiveau())/2             + 1    )
        {
            Defi defiNiveauSvt = this.manager.getDefiDuProchianNiveau();

            if (!defiNiveauSvt.estDebloque())
            {
                this.debloquerDefi( defiNiveauSvt.getNiveau(), defiNiveauSvt );
                this.profilManager.getProfilCourant().ajouterDefiFait(defiNiveauSvt.getNom());
            }
        }

        if (profilManager.getProfilCourant() != null)
        {
            this.profilManager.getProfilCourant().ajouterDefiFait(this.manager.getDefiCourant().getNom());
            this.profilManager.getProfilCourant().setScore(Defi.getNbCoups());
            this.profilManager.getProfilCourant().getFichierProfil().enregistrer();
        }
    }

    /**
     * Débloque le défi donné avec son niveau.
     * @param niveau
     * @param defiADebloque
     */
    private void debloquerDefi(Defi.Niveau niveau, Defi defiADebloque)
    {
        this.manager.ajouterNbCoupDuDefi(defiADebloque);

        defiADebloque.setEstDebloque(true);
        this.fenetre.getJeu().debloquerDefi( niveau, defiADebloque);
    }

    /**
     * Passe au niveau précédant si possible.
     */
    public void defiPrecedent()
    {

        if (this.manager.defiPrecedent())
        {
            this.fenetre.getJeu().reinitialiserCimetiere();
            this.fenetre.getJeu().getPlateau().setAide(false);
            this.fenetre.getJeu().update();
        }
    }

    /**
     * Passe au niveau suivant si possible.
     */
    public void defiSuivant()
    {
        if(this.manager.defiSuivant())
        {
            if(this.getAvertissement() == this.getIndiceDefiCourant())
            {
                Defi.setNbCoups(+2);
                this.setAvertissement(- 1);
            }
            this.fenetre.getJeu().getPlateau().setAide(false);
            this.fenetre.getJeu().reinitialiserCimetiere();
            this.fenetre.getJeu().update();
        }
    }

    public static void main(String[] args)
    {
        new Controleur();
    }

    /**
     * @return la couleur du  niveau de difficulté du défi courant
     */
    public Color getCouleurCourante()
    {
        Defi courant = this.manager.getDefiCourant();
        return courant == null ? null : courant.getNiveau().getCouleur();
    }

    /**
     * Getter.
     * @return le défi manager
     */
    public DefiManager getManager() {
        return this.manager;
    }

    /**
     * Getter.
     * @return le nom du défi
     */
    public String getNomDefi()
    {
        Defi courant = this.manager.getDefiCourant();
        return courant == null ? null : courant.getNom();
    }

    /**
     * Getter.
     * @return l'indice du défi courant
     */
    public int getIndiceDefiCourant() {
        return this.manager.getIndiceDefiCourant();
    }

    /**
     * Getter.
     * @return le défi courant
     */
    public Defi getDefiCourant()
    {
        return this.manager.getDefiCourant();
    }

    /**
     * Getter.
     * @return l'indice de l'avertissement (défi sur lequel on aura une pénalité)
     */
    public int getAvertissement() {
        return this.indiceAvert;
    }

    /**
     * Getter.
     * @param n dont on veut les défis
     * @return la liste des défis en fonction de leur niveau
     */
    public List<Defi> getListeDefis(Defi.Niveau n)
    {
        return this.manager.getDefis(n);
    }

    /**
     * Getter.
     * @return si l'aide autorisé
     */
    public boolean getAideAutorise() {
        return aideAutorise;
    }

    /**
     * Setter.
     * @param b
     */
    public void setAideAutorise(boolean b)
    {
        this.aideAutorise = b;
        this.gestionAide.restartTimer();
    }

    /**
     * Place le défi courant sur le défi dont l'indice est passé en paramètre.
     * @param indice
     */
    public void setDefiCourant(int indice)
    {
        this.manager.setDefiCourant( indice );
        this.fenetre.getJeu().reinitialiserCimetiere();
        this.fenetre.getJeu().update();
    }

    /**
     * Change l'indice de l'avertissement.
     * @param i
     */
    public void setAvertissement(int i) {
        this.indiceAvert = i;
    }

    /**
     * Méthode qui annule la dernière action effectuer.
     */
    public void annulerAction()
    {
        if (this.manager.annulerAction())
        {
            this.fenetre.getJeu().update();
            this.fenetre.getJeu().getCimetiere().supprimerDernier();
            this.fenetre.getJeu().getCimetiere().repaint();
        }
    }

    /**
     * Utilisé pour la gestion du débloquage au bout de 50%.
     * @return si il y a un défi débloqué après le courant
     */
    public boolean aUnDefiSuivantDebloque()
    {
        return this.manager.aUnDefiSuivantDbloque();
    }

    /**
     * @return la liste des profils chargés
     */
    public String[] getListeProfil()
    {
        return this.profilManager.getListeNom();
    }

    /**
     * Crée un nouveau profil.
     *
     * @param nom le nom du profil
     * @return vrai si le profil a pu être créé
     */
    public boolean creerProfil(String nom)
    {
        if (this.profilManager.creerProfil(nom))
        {
            this.fenetre.afficherAccueil();
            return true;
        }

        return false;
    }

    /**
     * Joue avec le profil sélectionné de la liste des profils.
     */
    public void jouer()
    {
        String nom = this.fenetre.getAccueil().getProfilSelectionne();

        if (nom != null)
        {
            if ( this.profilManager.chargerProfil( nom ) )
            {
                this.manager = new DefiManager( this.profilManager.getProfilCourant() );
                this.fenetre.afficherJeu();

                this.gestionAide = new GestionnaireAide( this, this.fenetre.getJeu().getPlateau() );

                for (Defi d : this.manager.getDefis())
                    this.fenetre.getJeu().debloquerDefi( d.getNiveau(), d);
            }
        }
    }

    /**
     * Aide l'utilisateur.
     */
    public void aider()
    {
        this.fenetre.getJeu().getPlateau().proposerAide();
    }

    /**
     * Crée une nouvelle partie sur le profil sélectionné
     * de la liste des profils
     */
    public void creerNouvellePartie()
    {
        String nom = this.fenetre.getAccueil().getProfilSelectionne();

        if (nom != null)
        {
            this.profilManager.chargerProfil( this.fenetre.getAccueil().getProfilSelectionne() );

            this.profilManager.getProfilCourant().reinitialiser();
            this.manager = new DefiManager(this.profilManager.getProfilCourant());

            this.jouer();
        }
    }

    /**
     * Getter.
     * @return le gestionnaire d'aide
     */
    public GestionnaireAide getGestionAide()
    {
        return gestionAide;
    }

    /**
     * Supprime le profil sélectionné de la liste des profils.
     */
    public void supprimerProfil()
    {
        String nom = this.fenetre.getAccueil().getProfilSelectionne();

        if (nom != null)
        {
            this.profilManager.chargerProfil( nom );
            this.profilManager.getProfilCourant().getFichierProfil().supprimer();

            this.profilManager = new ProfilManager();

            this.fenetre.afficherAccueil();
        }
    }

    public boolean updateProfil( String nom )
    {
        if (this.profilManager.chargerProfil( this.fenetre.getAccueil().getProfilSelectionne() ))
        {
            this.profilManager.getProfilCourant().setNom(nom);
            this.fenetre.afficherAccueil();
            return true;
        }

        return false;
    }
}
