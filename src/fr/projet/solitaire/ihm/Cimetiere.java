package fr.projet.solitaire.ihm;

import fr.projet.solitaire.Controleur;
import fr.projet.solitaire.ihm.graphic.IGrille;
import fr.projet.solitaire.ihm.graphic.ImageException;
import fr.projet.solitaire.ihm.graphic.PieceForme;
import fr.projet.solitaire.metier.pieces.Piece;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Classe qui gere le "Cimetiere" de pièces de l'interface.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Cimetiere extends JPanel implements IGrille
{
    private List<PieceForme> cimetiere;

    /**
     * Construit un cimetière de pièces.
     */
    public Cimetiere()
    {
        this.cimetiere = new ArrayList<PieceForme>();
    }

    // Redéfinition des GETTERS.
    @Override public int getOrigineX() { return this.getWidth() / 10;   }
    @Override public int getOrigineY() { return 0;                      }
    @Override public int getTaille()   { return this.getOrigineX() * 9; }

    /**
     * Récupère la taille des cases.
     *
     * @return Taille des cases.
     */
    @Override
    public int getTailleCase()
    {
        int taille = (this.getTaille() - this.getOrigineX()) / 8;
        int tMax   = (this.getHeight() * 3 / 4) / 2;
        return taille > tMax ? tMax : taille;
    }

    /**
     * Dessine l'ensemble des pièces du cimetière.
     *
     * @param g Objet de dessin graphique.
     */
    @Override
    public void dessiner(Graphics g)
    {
        if (this.cimetiere.size() == 0)
            return;

        for (PieceForme piece : this.cimetiere)
            piece.dessiner(g);
    }

    /**
     * Repaint le composant.
     *
     * @param g Objet de dessin graphique.
     */
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        this.dessiner(g);
    }

    public void update()
    {
        this.repaint();
    }

    /**
     * Ajoute une pièce au cimetière.
     *
     * @param piece Pièce à ajouter.
     * @return      true si elle est ajouté.
     */
    public boolean ajouterTue(Piece piece)
    {
        if (piece == null)
            return false;

        try {
            PieceForme forme = new PieceForme.Cimetiere
                    (
                            this                    ,
                            piece                   ,
                            this.cimetiere.size() - 1 // Position dans la grille.
                    );

            this.cimetiere.add(forme);
        } catch (ImageException e) {
            e.printStackTrace();
            return false;
        }

        this.repaint();
        return true;
    }

    /**
     * Vide le cimetière.
     */
    public void reinitialiser()
    {
        this.cimetiere.clear();
        this.repaint();
    }

    /**
     * Supprime la dernière pièce du cimetière.
     *
     * @return true si supprimée, sinon false.
     */
    public boolean supprimerDernier()
    {
        if (this.cimetiere.size() == 0)
            return false;

        this.cimetiere.remove(this.cimetiere.size() - 1);
        return true;
    }
}
