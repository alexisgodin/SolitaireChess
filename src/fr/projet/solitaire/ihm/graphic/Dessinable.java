package fr.projet.solitaire.ihm.graphic;

import java.awt.*;

/**
 * Forme graphique à dessiner.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public interface Dessinable
{
    /**
     * Dessine une forme sur un composant graphique.
     *
     * @param g Objet de dessin graphique.
     */
    void dessiner(Graphics g);
}
