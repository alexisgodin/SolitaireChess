package fr.projet.solitaire.ihm.graphic;


import fr.projet.solitaire.metier.Position;
import fr.projet.solitaire.metier.pieces.Piece;

import java.awt.*;

/**
 * Représente un tablier avec ses pièces.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Tablier extends Grille
{
    private Group     group;
    private Piece[][] pieces;

    /**
     * Créé la grille avec la taille donnée et son
     * nombre de cases.
     *
     * @param pieces  Pièces du tablier.
     */
    public Tablier(Piece[][] pieces, int tailleCase)
    {
        super(tailleCase, pieces.length);
        this.group  = new Group();
        this.pieces = pieces;

        this.group.ajouter(Group.COUCHE_MAX, this.dessinPieces());
    }

    // GETTERS
    @Override public int getOrigineX() { return 0;           }
    @Override public int getOrigineY() { return 0;           }

    public Group         getGroup()    { return this.group;  }
    public Piece[][]     getPieces()   { return this.pieces; }

    /**
     *
     * @param pos
     * @return
     */
    public Piece getPiece(Position pos)
    {
        return pos == null ? null : this.getPiece(pos.getX(), pos.getY());
    }

    /**
     * Récupère la pièce situé sur la case donnée,
     * si la case est hors du tablier, alors renvois null.
     *
     * @param caseX Position en X de la case.
     * @param caseY Position en Y de la case.
     * @return      La pièce à la case demandé ou null si
     *              elle est vide ou les paramètres ne sont
     *              pas valides.
     */
    public Piece getPiece(int caseX, int caseY)
    {
        if (    caseX < 0 || caseX >= this.pieces.length ||
                caseY < 0 || caseY >= this.pieces.length    )
            return null;

        return this.pieces[caseY][caseX];
    }

    /**
     *
     * @param pieces
     */
    public void setPieces(Piece[][] pieces)
    {
        if (pieces.length != getNbCases())
            throw new IllegalArgumentException("Le tableau n'a pas le bon nombre de cases.");

        this.pieces = pieces;
    }

    /**
     *
     * @param piece
     * @param caseX
     * @param caseY
     */
    public void setPiece(Piece piece, int caseX, int caseY)
    {
        if (    caseX < 0 || caseX >= this.pieces.length ||
                caseY < 0 || caseY >= this.pieces.length    )
            return;

        this.pieces[caseY][caseX] = piece;
    }

    /**
     *
     * @param g Objet de dessin graphique.
     */
    @Override
    public void dessiner(Graphics g)
    {
        super.dessiner(g);
        this.group.dessiner(g);
    }

    /**
     *
     * @return
     */
    private Dessinable dessinPieces()
    {
        return new Dessinable()
        {
            @Override
            public void dessiner(Graphics g)
            {
                for(int caseX = 0; caseX < pieces.length; caseX++)
                {
                    for (int caseY = 0; caseY < pieces.length; caseY++)
                    {
                        if (masquer(caseX, caseY))
                            continue;

                        Piece piece = getPiece(caseX, caseY);
                        if (piece == null)
                            continue;

                        try
                        {
                            new PieceForme.Tablier(Tablier.this, piece, caseX, caseY).dessiner(g);
                        } catch (ImageException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
    }

    /**
     * Permet de masquer une piece depuis une classe fille.
     * Si elle est masquée, alors elle ne sera pas représenté graphiquement.
     *
     * @param caseX Case en X à masquer.
     * @param caseY Case en Y à masquer.
     * @return      true si masquée.
     */
    public boolean masquer(int caseX, int caseY)
    {
        return false;
    }
}
