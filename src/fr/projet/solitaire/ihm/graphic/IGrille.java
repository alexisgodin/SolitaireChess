package fr.projet.solitaire.ihm.graphic;

/**
 * Forme d'une grille qui peut-être dessiné.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public interface IGrille extends Dessinable
{
    /**
     * Position en X de la grille.
     * Elle est située dans le coin de gauche.
     *
     * @return Position X de la grille.
     */
    int getOrigineX();

    /**
     * Position en Y de la grille.
     * Elle est située dans le coin en haut.
     *
     * @return Position Y de la grille.
     */
    int getOrigineY();

    /**
     * Récupère la taille de la grille.
     *
     * @return Taille de la grille.
     */
    int getTaille();

    /**
     * Récupère la taille d'une case.
     *
     * @return Taille d'une case.
     */
    int getTailleCase();
}
