package fr.projet.solitaire.ihm.graphic;

import fr.projet.solitaire.metier.pieces.Piece;

import java.awt.*;

/**
 * Représente une pièce sous forme graphique.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class PieceForme extends ImageForme
{
    public  static final float  ZOOM      = 1.5F;
    private static final String EXTENSION = ".gif";

    private Piece piece;

    /**
     * Construit une pièce graphiquement sans dimension ni
     * position.
     *
     * @param piece Pièce à dessiner.
     * @throws ImageException Si l'image n'existe pas.
     */
    public PieceForme(Piece piece) throws ImageException
    {
        this(piece, 0, 0, 0, 0);
    }

    /**
     * Construit une pièce graphiquement avec les
     * paramètres donnés.
     *
     * @param piece   Pièce à dessiner.
     * @param posX    Position d'origine en X.
     * @param posY    Position d'origine en Y
     * @param tailleX Taille du dessin en X.
     * @param tailleY Taille du dessin en Y.
     * @throws ImageException Si l'image n'existe pas.
     */
    public PieceForme(Piece piece, int posX, int posY,
                      int tailleX, int tailleY) throws ImageException
    {
        super(PieceForme.getNomImage(piece), posX, posY, tailleX, tailleY);
        this.piece = piece;
    }

    /**
     * Récupère la pièce liée à la forme.
     *
     * @return La pièce de la forme.
     */
    public Piece getPiece()
    {
        return this.piece;
    }

    @Override
    public PieceForme clone()
    {
        return (PieceForme) super.clone();
    }

    /**
     * Récupère le nom de la pièce en ce basant sur
     * le nom de sa classe.
     *
     * @param piece Pièce à qui récupérrer le nom.
     * @return      Nom et extension de l'image.
     */
    private static String getNomImage(Piece piece)
    {
        return piece == null ?
                null :
                piece.getClass().getSimpleName().toLowerCase() + PieceForme.EXTENSION;
    }


    /* ---------------------- */
    /* PLATEAU                */
    /* ---------------------- */

    /**
     * Représentation d'une pièce sur un tablier.
     */
    public static class Tablier extends PieceForme
    {
        /**
         * Construit une pièce placeable sur un plateau.
         *
         * @param grille Tableau où dessiner la pièce.
         * @param piece  Pièce à dessiner.
         * @param caseX  Case en X ciblée du tablier.
         * @param caseY  Case en Y ciblée du tablier.
         * @throws ImageException Si l'image n'existe pas.
         */
        public Tablier(IGrille grille, Piece piece, int caseX, int caseY) throws ImageException
        {
            super(
                    piece,
                    grille.getOrigineX() + grille.getTailleCase() * caseX,  // x
                    grille.getOrigineY() + grille.getTailleCase() * caseY,  // y
                    grille.getTailleCase() - 2                           ,  // width
                    grille.getTailleCase() - 2                            );// height
        }
    }

    /* ---------------------- */
    /* CIMETIERE              */
    /* ---------------------- */

    /**
     * Représentation d'une pièce dans le cimetière.
     */
    public static class Cimetiere extends PieceForme
    {
        /**
         * Construit une pièce placeable dans un cimetière.
         *
         * @param grille Tableau où dessiner la pièce.
         * @param piece  Pièce à dessiner.
         * @param pos    Position dans la grille.
         * @throws ImageException Si l'image n'existe pas.
         */
        public Cimetiere(IGrille grille, Piece piece, int pos) throws ImageException
        {
            super(
                    piece,
                    grille.getOrigineX() + grille.getTailleCase() * (pos % 8) + 1, // x
                    grille.getOrigineY() + grille.getTailleCase() *  pos / 8     , // y
                    grille.getTailleCase() - 2                                   , // width
                    grille.getTailleCase() - 2                                     // height
            );
        }
    }
}
