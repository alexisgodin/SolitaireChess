package fr.projet.solitaire.ihm.graphic;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Représentation de l'icon d'aide.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class AideForme extends ImageForme
{
    /**
     * Extension de l'image
     */
    private static final String EXTENSION = ".gif";

    /**
     * Construit une forme d'image à partir des paramètres donné.
     *
     * @param origineX Position d'origine en X.
     * @param origineY Position d'origine en Y
     * @param tailleX  Taille du dessin en X.
     * @param tailleY  Taille du dessin en Y.
     * @throws ImageException si l'image n'existe pas.
     */
    public AideForme(int origineX, int origineY, int tailleX, int tailleY) throws ImageException
    {
        super(AideForme.getNomImage(), origineX, origineY, tailleX, tailleY);
    }

    /**
     * Récupère une image d'une pièce.
     *
     * @return      L'image associé à la pièce.
     * @throws ImageException Si l'image n'existe pas.
     */
    public static Image getImage() throws ImageException
    {
        return ImageForme.getImage(AideForme.getNomImage());
    }

    /**
     * Récupère le nom de la pièce en ce basant sur
     * le nom de sa classe.
     *
     * @return      Nom et extension de l'image.
     */
    private static String getNomImage()
    {
        return "ampoule" + AideForme.EXTENSION;
    }
}
