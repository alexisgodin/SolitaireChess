package fr.projet.solitaire.ihm.graphic;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;

/**
 * Image sous forme graphique.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
abstract public class ImageForme implements Dessinable, Cloneable
{
    private Image image;

    private int   posX;
    private int   posY;

    private int   tailleX;
    private int   tailleY;

    private int   offsetX;
    private int   offsetY;

    /**
     * Construit une forme graphique d'une image.
     *
     * @param nomFichier Nom du fichier avec son extension.
     * @param posX       Position d'origine en X.
     * @param posY       Position d'origine en Y
     * @param tailleX    Taille sur l'axe X de l'image.
     * @param tailleY    Taille sur l'axe Y de l'image.
     * @throws ImageException Si l'image n'existe pas.
     */
    public ImageForme(String nomFichier,
                      int posX, int posY,
                      int tailleX, int tailleY) throws ImageException
    {
        this.image   = ImageForme.getImage(nomFichier);
        this.posX    = posX;
        this.posY    = posY;
        this.tailleX = tailleX;
        this.tailleY = tailleY;
    }

    // GETTERS
    public int  getPosX()               { return this.posX;       }
    public int  getPosY()               { return this.posY;       }
    public int  getTailleX()            { return this.tailleX;    }
    public int  getTailleY()            { return this.tailleY;    }
    public int  getOffsetX()            { return this.offsetX;    }
    public int  getOffsetY()            { return this.offsetY;    }

    // SETTERS
    public void setTailleX(int tailleX) { this.tailleX = tailleX; }
    public void setTailleY(int tailleY) { this.tailleY = tailleY; }
    public void setOffsetX(int offsetX) { this.offsetX = offsetX; }
    public void setOffsetY(int offsetY) { this.offsetY = offsetY; }

    /**
     * Déplace l'image à la position donnée.
     *
     * @param posX Nouvelle position X.
     * @param posY Nouvelle position Y.
     */
    public void deplacer(int posX, int posY)
    {
        this.posX = posX;
        this.posY = posY;
    }

    /**
     * Modifie la taille de l'image.
     *
     * @param zoom Zoom compris entre 0 et Float.MAX_VALUE.
     */
    public void zoom(float zoom)
    {
        if (zoom < 0)
            throw new IllegalArgumentException("Zoom négatif");

        int tmpTailleX = this.tailleX;
        int tmpTailleY = this.tailleY;

        this.tailleX *= zoom;
        this.tailleY *= zoom;

        this.posX -= (this.tailleX - tmpTailleX) / 2;
        this.posY -= (this.tailleY - tmpTailleY) / 2;
    }

    /**
     * Dessine l'image de la pièce avec les attributs donné.
     *
     * @param g Objet de dessin graphique.
     */
    @Override
    public void dessiner(Graphics g) {
        g.drawImage
                (
                        this.image                           ,
                        this.getPosX() - this.offsetX        ,
                        this.getPosY() - this.offsetY        ,
                        this.getTailleX() , this.getTailleY(),
                        null
                );
    }

    @Override
    public boolean equals(Object obj)
    {
        return obj == this || obj instanceof ImageForme && this.image.equals(((ImageForme) obj).image);
    }

    @Override
    public ImageForme clone()
    {
        try
        {
            return (ImageForme) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    /**
     * Récupère une image dans le dossier images à partir
     * de son nomn.
     *
     * @param nomFichier Nom du fichier avec son extension.
     * @return           L'image associé au nom.
     * @throws ImageException Si l'image n'existe pas.
     */
    static Image getImage(String nomFichier) throws ImageException
    {
        try
        {
            return ImageIO.read(new File("images/" + nomFichier));
        }
        catch(Exception ignored)
        {
            throw new ImageException(nomFichier);
        }
    }
}
