package fr.projet.solitaire.ihm.graphic;

import java.awt.*;

/**
 * Représentation d'une grille quelconque sans
 * position dans l'espace.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
abstract public class Grille implements IGrille
{
    private Color couleurLigne;
    private Color couleurPair;
    private Color couleurImpair;

    private int   taille;
    private int   nbCases;

    /**
     * Créé la grille avec la taille donnée et son
     * nombre de cases.
     *
     * @param tailleCase  Taille d'une case..
     * @param nbCases     Nombre de cases.
     *                    (Est le même sur les 2 axes)
     */
    public Grille(int tailleCase, int nbCases) {
        this.taille  = tailleCase * nbCases;
        this.nbCases = nbCases;

        this.couleurLigne  = Color.BLACK;
        this.couleurPair   = Color.WHITE;
        this.couleurImpair = Color.BLACK;
    }

    // GETTERS
    @Override public    int getTaille()     { return this.taille;                }
    @Override public    int getTailleCase() { return this.taille / this.nbCases; }
              protected int getNbCases()    { return this.nbCases;               }

    // SETTERS
    public void setCouleurLigne (Color couleurLigne)  { this.couleurLigne  = couleurLigne;  }
    public void setCouleurImpair(Color couleurImpair) { this.couleurImpair = couleurImpair; }
    public void setCouleurPair  (Color couleurPair)   { this.couleurPair   = couleurPair;   }

    /**
     * Dessine la grille avec les paramètres de taille et
     * couleur de l'instance.
     *
     * @param g Objet de dessin graphique.
     */
    @Override
    public void dessiner(Graphics g)
    {
        int origineX = this.getOrigineX();
        int origineY = this.getOrigineY();

        // Lignes
        g.setColor(this.couleurLigne);
        for(int i = 0; i <= this.nbCases; i++)
        {
            // Ligne vertical
            g.drawLine(
                    origineX + this.getTailleCase() * i,
                    origineY                           ,
                    origineX + this.getTailleCase() * i,
                    origineY + this.getTaille()         );

            // Ligne horizontal
            g.drawLine(
                    origineX                           ,
                    origineY + this.getTailleCase() * i,
                    origineX + this.getTaille()        ,
                    origineY + this.getTailleCase() * i );
        }

        // Carreaux
        for(int x = 0; x < this.nbCases; x++)
        {
            for (int y = 0; y < this.nbCases; y++)
            {
                if ((x + y) % 2 == 1)
                    g.setColor(this.couleurPair);
                else
                    g.setColor(this.couleurImpair);

                g.fillRect(
                        origineX + 1 + this.getTailleCase() * x,
                        origineY + 1 + this.getTailleCase() * y,
                        this.getTailleCase() - 1,
                        this.getTailleCase() - 1);
            }
        }
    }
}
