package fr.projet.solitaire.ihm.graphic;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Groupe d'objets graphique.
 * Cet objet est lui même dessinable.
 *
 * Permet de gérer la couche d'affichage de chaque objet.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Group implements Dessinable
{
    public static final int COUCHE_MAX = 5;

    private Map<Integer, List<Dessinable>> dessinables;

    /**
     * Construit un objet qui va stocker d'autres objets
     * dessinables.
     */
    public Group()
    {
        this.dessinables = new HashMap<Integer, List<Dessinable>>();
    }

    /**
     * Ajoute un élément dessinable à une couche du group. La couche
     * doit être comprise entre 0 et COUCHE_MAX.
     *
     * @param couche     Niveau de priorité, plus la couche est élevé,
     *                   plus l'élément graphique sera mis en avant sur
     *                   l'interface graphique.
     * @param dessinable Element à dessiner.
     */
    public void ajouter(int couche, Dessinable dessinable)
    {
        if (couche < 0)
            throw new IllegalArgumentException("La couche ne peut pas être inférieur à 0");

        if (couche > Group.COUCHE_MAX)
            throw new IllegalArgumentException("La couche ne peut pas être supérieur à " +
                                               Group.COUCHE_MAX);

        if (dessinable == this)
            throw new IllegalArgumentException("Le groupe ne peut pas se contenir !");

        if (!this.dessinables.containsKey(couche))
        {
            this.dessinables.put(couche, new LinkedList<Dessinable>());
        }

        this.dessinables.get(couche).add(dessinable);
    }

    /**
     * Supprime une couche entière, tous les éléments de la
     * couche seront supprimés graphiquement.
     *
     * @param couche Couche à supprimer.
     */
    public void retirerCouche(int couche)
    {
        this.dessinables.remove(couche);
    }

    /**
     * Dessine l'ensemble des éléments graphique.
     *
     * @param g Objet de dessin graphique.
     */
    @Override
    public void dessiner(Graphics g)
    {
        for (int couche = 0; couche <= Group.COUCHE_MAX; couche++)
        {
            if (this.dessinables.containsKey(couche))
            {
                for (Dessinable d : this.dessinables.get(couche))
                    d.dessiner(g);
            }
        }
    }
}
