package fr.projet.solitaire.ihm.graphic;

/**
 * Exception déclenché lors d'un mauvais
 * charger d'image du répertoire images.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class ImageException extends Exception {

    private String emplacement;

    /**
     * Construit une exception d'image pour afficher
     * dans la console le chemin vers l'image demandée.
     *
     * @param emplacement Emplacement de l'image du répertoire images.
     */
    public ImageException(String emplacement)
    {
        super("Probleme de chargement de l'image : images/" + emplacement);
        this.emplacement = "images/" + emplacement;
    }

    /**
     * Récupère l'emplacement de l'image en se basant
     * sur le répertoire images.
     *
     * @return L'emplacement de l'image.
     */
    public String getEmplacement() {
        return this.emplacement;
    }
}
