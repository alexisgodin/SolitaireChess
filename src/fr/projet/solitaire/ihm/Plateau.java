package fr.projet.solitaire.ihm;

import fr.projet.solitaire.Controleur;
import fr.projet.solitaire.ihm.graphic.*;
import fr.projet.solitaire.metier.Defi;
import fr.projet.solitaire.metier.Position;
import fr.projet.solitaire.metier.pieces.Piece;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Classe de gestion du plateau de jeu.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Plateau extends JPanel implements MouseMotionListener, MouseListener, ActionListener
{
    private static final Color ZONE_PION      = new Color(30, 122, 75);
    private static final Color ZONE_SANS_PION = new Color(232, 81, 72);

    private static final Color AIDE_DEPART    = new Color(95, 164, 255);
    private static final Color AIDE_ARRIVEE   = new Color(75, 33, 232);

    private Fenetre        fenetre;
    private PlateauTablier tablier;
    private PieceForme     selectPiece;

    private JDialog        dialog;
    private boolean        aide;

    /**
     * Construit un plateau de jeu dimensionné au milieu de la fenêtre.
     *
     * @param fenetre Fenêtre du plateau.
     */
    public Plateau(Fenetre fenetre)
    {
        this.fenetre = fenetre;

        this.setSize(fenetre.getWidth(), fenetre.getHeight() * 5 / 6);
        this.setPreferredSize(new Dimension(this.getWidth(), this.getHeight()));
        this.setPieces(fenetre.getControleur().getPieces());

        this.addMouseListener(this);
        this.addMouseMotionListener(this);

        this.repaint();
    }

    // GETTER/SETTER
    public boolean getAide()          { return this.aide; }
    public void    setAide(boolean b) { this.aide = b;    }

    /**
     * Définir les pièces du tablier.
     *
     * @param pieces Ensemble des pièces du tablier.
     * @throws IllegalArgumentException Si le tableau a une taille de 0 ou
     *                                  si il n'est pas carré.
     */
    public void setPieces(Piece[][] pieces)
    {
        if (pieces.length == 0)
            throw new IllegalArgumentException("Le tableau ne doit pas avoir une taille null.");

        if (pieces.length != pieces[0].length)
            throw new IllegalArgumentException("Le tablier doit être carré");

        this.tablier = new PlateauTablier(pieces);
        this.tablier.setCouleurImpair(this.fenetre.getControleur().getCouleurCourante());
        this.tablier.setCouleurPair(Color.WHITE);

        this.repaint();
    }

	/**
     * Actualise le plateau.
     */
    public void update() { this.repaint(); }

    /**
     * Repaint le tablier.
     *
     * @param g Objet de dessin graphique.
     */
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        this.tablier.dessiner(g);

        if (this.selectPiece != null)
            this.selectPiece.dessiner(g);

        Controleur ctrl = this.fenetre.getControleur();
        if (ctrl.getDefiCourant().defiPossedeAide() && !this.aide)
            ctrl.getGestionAide().dessiner(
                    g                         ,
                    this.getWidth() / 5 * 4   ,
                    this.getHeight() / 2      ,
                    PlateauTablier.TAILLE_CASE );
    }

    /**
     *
     */
    public void creerOptionPane()
    {
        JButton bOui, bNon;
        bOui = new JButton("Oui");
        bOui.addActionListener(this);
        bNon = new JButton("Non");
        bNon.addActionListener(this);
        Object[] options = { bOui, bNon };

        JOptionPane optionsPane = new JOptionPane();
        optionsPane.setMessage("Voulez-vous vraiment obtenir de l'aide ?" + "\n" +
                               "Cela vous coûtera 4 déplacements.");
        optionsPane.setMessageType(JOptionPane.WARNING_MESSAGE);
        optionsPane.setOptions(options);

        this.dialog = optionsPane.createDialog(null, "Attention");
    }

    /**
     *
     */
    public void proposerAide()
    {
        if(!this.aide) {
            this.creerOptionPane();
            this.dialog.setVisible(true);
        }
    }

    /**
     *
     */
    public void aider()
    {
        this.aide = true;
        Defi.setNbCoups(+ 4);
        this.dialog.dispose();
        this.fenetre.getJeu().update();
        this.repaint();
    }

    /**
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() instanceof JButton) {
            if(((JButton)e.getSource()).getText().equals("Oui"))
                this.aider();

            if(((JButton)e.getSource()).getText().equals("Non"))
                this.dialog.dispose();
        }
    }

    /* --------------------------- */
    /* Tablier                     */
    /* --------------------------- */

    /**
     * Classe qui gère un tablier sous forme graphique..
     */
    private class PlateauTablier extends Tablier
    {
        private static final int TAILLE_CASE = 52;

        private Position selectCase;

        /**
         * Construit un tablier à partir d'un tableau
         * de pièces.
         *
         * @param pieces Pièces du tablier.
         */
        public PlateauTablier(Piece[][] pieces)
        {
            super(pieces, PlateauTablier.TAILLE_CASE);
            this.getGroup().ajouter(1, this.aideAfficheur());
            this.getGroup().ajouter(1, this.deplacementAfficheur());
        }

        // GETTER
                  public Position getSelectCase() { return this.selectCase;                                   }
        @Override public int      getOrigineX()   { return (Plateau.this.getWidth()  - this.getTaille()) / 2; }
        @Override public int      getOrigineY()   { return (Plateau.this.getHeight() - this.getTaille()) / 2; }

        // SETTER
        public void setSelectCase(Position selectCase) { this.selectCase = selectCase; }

        /**
         * Masque la case si elle est selectionnée.
         *
         * @param caseX Case en X à masquer.
         * @param caseY Case en Y à masquer.
         * @return      true si masquée.
         */
        @Override
        public boolean masquer(int caseX, int caseY)
        {
            return this.selectCase != null          &&
                    caseX == this.selectCase.getX() &&
                    caseY == this.selectCase.getY();
        }

        /**
         *
         * @return
         */
        private Dessinable deplacementAfficheur()
        {
            return new Dessinable()
            {
                @Override
                public void dessiner(Graphics g)
                {
                    if (selectPiece == null)
                        return;

                    int selectX = selectCase.getX();
                    int selectY = selectCase.getY();

                    int tailleCase = getTailleCase();
                    for (Position deplacement : selectPiece.getPiece().getDeplacementPossible(
                            getPieces(), selectX, selectY))
                    {
                        if (getPiece(deplacement) == null)
                            g.setColor(Plateau.ZONE_SANS_PION);
                        else
                            g.setColor(Plateau.ZONE_PION);

                        g.fillRect(
                                getOrigineX() + deplacement.getX() * tailleCase + 1,
                                getOrigineY() + deplacement.getY() * tailleCase + 1,
                                tailleCase - 1,
                                tailleCase - 1);
                    }
                }
            };
        }

        /**
         *
         * @return
         */
        private Dessinable aideAfficheur()
        {
            return new Dessinable()
            {
                @Override
                public void dessiner(Graphics g)
                {
                    if(!aide)
                        return;

                    Position[] aides = fenetre.getControleur().getDefiCourant().getAide();
                    if(aides == null || aides.length != 2)
                        return;

                    g.setColor(Plateau.AIDE_DEPART);
                    g.fillOval
                            (
                                    getOrigineX() + 1 + getTailleCase() * aides[0].getX(),
                                    getOrigineY() + 1 + getTailleCase() * aides[0].getY(),
                                    getTailleCase() - 1                                  ,
                                    getTailleCase() - 1
                            );

                    g.setColor(Plateau.AIDE_ARRIVEE);
                    g.fillRect
                            (
                                    getOrigineX() + 1 + getTailleCase() * aides[1].getX(),
                                    getOrigineY() + 1 + getTailleCase() * aides[1].getY(),
                                    getTailleCase() - 1                                  ,
                                    getTailleCase() - 1
                            );
                }
            };
        }
    }

    /**
     * Convertie un point sur l'écran dans l'échelle du tablier.
     *
     * @param x Position à convertir.
     * @return  Une case.
     */
    private int coordXToCase(int x)
    {
        return (x - this.tablier.getOrigineX()) / this.tablier.getTailleCase();
    }

    /**
     * Convertie un point sur l'écran dans l'échelle du tablier.
     *
     * @param y Position à convertir.
     * @return  Une case.
     */
    private int coordYToCase(int y)
    {
        return (y - this.tablier.getOrigineY()) / this.tablier.getTailleCase();
    }

    /* -------------------------- */
    /* Evènements                 */
    /* -------------------------- */

    /**
     * Déclenché quand un clique est effectué.
     * Permet de selectionner une pièce dans le
     * tablier.
     *
     * @param e Evenement lié à la souris.
     */
    @Override
    public void mousePressed(MouseEvent e)
    {
        this.fenetre.getControleur().getGestionAide().help(e.getX(), e.getY(), PlateauTablier.TAILLE_CASE);

        if (this.selectPiece != null)
            return;

        int selectX = this.coordXToCase(e.getX());
        int selectY = this.coordYToCase(e.getY());
        this.tablier.setSelectCase(new Position(selectX, selectY));

        Piece piece = this.tablier.getPiece(selectX, selectY);
        if (piece == null)
            return;

        try
        {
            this.selectPiece = new PieceForme.Tablier
                    (
                            this.tablier                           ,
                            this.tablier.getPiece(selectX, selectY),
                            selectX,
                            selectY
                    );
            this.selectPiece.zoom(PieceForme.ZOOM);
            this.selectPiece.setOffsetX(e.getX() - this.selectPiece.getPosX());
            this.selectPiece.setOffsetY(e.getY() - this.selectPiece.getPosY());
            this.selectPiece.deplacer(e.getX(), e.getY());

            this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        } catch (ImageException ex)
        {
            this.selectPiece = null;
            this.tablier.setSelectCase(null);
        }

        this.repaint();
    }

    /**
     * Déclenché quand le clique de la souris est
     * relâché. Nettoie la selectionné lié au glissé
     * déposé et déplace la pièce voulue.
     *
     * @param e Evenement lié à la souris.
     */
    @Override
    public void mouseReleased(MouseEvent e)
    {
        if (this.selectPiece != null)
        {
            if (this.getMousePosition() != null)
            {
                this.fenetre.getControleur().deplacer
                        (
                                this.tablier.getSelectCase().getX()         ,
                                this.tablier.getSelectCase().getY()         ,
                                this.coordXToCase(this.getMousePosition().x),
                                this.coordYToCase(this.getMousePosition().y)
                        );
            }
            this.setCursor(Cursor.getDefaultCursor());

            this.selectPiece = null;
            this.tablier.setSelectCase(null);

            this.repaint();
        }
    }

    /**
     * Déclenché en déplaçant sa souris avec le clique
     * enfoncé. Si une pièce est selectionnée, alors
     * actualise sa position graphiquement.
     *
     * @param e Evenement lié à la souris.
     */
    @Override
    public void mouseDragged(MouseEvent e)
    {
        if (this.selectPiece != null)
            this.selectPiece.deplacer(e.getX(), e.getY());

        this.repaint();
    }

    // Inutile
    @Override
    public void mouseMoved  (MouseEvent e) {}
    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited (MouseEvent e) {}
}
