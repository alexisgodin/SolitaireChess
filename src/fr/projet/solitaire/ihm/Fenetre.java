package fr.projet.solitaire.ihm;

import fr.projet.solitaire.Controleur;
import fr.projet.solitaire.ihm.menus.BarreMenus;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Classe de gestion de la fenêtre d'interface utilisateur.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */

public class Fenetre extends JFrame
{
    /**
     * Controleur.
     */
    private Controleur ctrl;

    /**
     * Classe qui affiche la grille de jeu et qui permzet de jouer.
     */
    private Jeu jeu;

    /**
     * Classe qui affiche l'accueil.
     */
    private Accueil accueil;

    /**
     * Constructeur.
     * @param ctrl
     */
    public Fenetre(Controleur ctrl)
    {
        this.ctrl = ctrl;

        this.setTitle("Solitaire Chess");
        BufferedImage icon;
        try
        {
            icon = ImageIO.read(new File("images/reine.gif"));
            this.setIconImage(icon);
        }
        catch(Exception ignored) {}

        this.setSize(500,600);
        this.setMinimumSize(new Dimension(400,600)); // TODO
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.jeu = new Jeu(this);

        this.accueil = new Accueil(ctrl);
        this.add(this.accueil);

        this.setJMenuBar( new BarreMenus(this) );

        ((BarreMenus) this.getJMenuBar()).getMenuFichier().activationMenuFichier(false);

        this.setVisible(true);

    }

    /**
     * Getter.
     * @return le controleur
     */
    public Controleur getControleur()
    {
        return this.ctrl;
    }

    /**
     * Getter.
     * @return l'accueil
     */
    public Accueil getAccueil() {
        return accueil;
    }

    /**
     * Getter.
     * @return le panel de jeu
     */
    public Jeu getJeu() { return jeu; }

    /**
     * Affiche l'accueil.
     */
    public void afficherAccueil()
    {
        if (this.accueil != null)
            this.remove( this.accueil );

        if (this.jeu != null)
            this.remove( this.jeu );

        ((BarreMenus) this.getJMenuBar()).activationMenuDefi(false);
        ((BarreMenus) this.getJMenuBar()).getMenuFichier().activationMenuFichier(false);

        this.accueil = new Accueil( this.ctrl );
        this.add( this.accueil );
        this.getContentPane().validate();
        this.repaint();
    }

    /**
     * Affiche la grille de jeu et permet de jouer.
     */
    public void afficherJeu()
    {
        if (this.accueil != null)
            this.remove( this.accueil );

        if (this.jeu != null)
            this.remove( this.jeu );

        ((BarreMenus) this.getJMenuBar()).activationMenuDefi(true);
        ((BarreMenus) this.getJMenuBar()).getMenuFichier().activationMenuFichier(true);

        this.jeu = new Jeu( this );
        this.add( this.jeu );
        this.getContentPane().validate();
        this.repaint();
    }
}
