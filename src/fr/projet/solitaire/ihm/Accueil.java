package fr.projet.solitaire.ihm;

import fr.projet.solitaire.Controleur;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe d'affichage de l'accueil du jeu.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Accueil extends JPanel
{
    /**
     * Le controleur.
     */
    private Controleur ctrl;

    /**
     * Pannels de l'accueil.
     */
    private JPanel pHaut, pGauche, pCentre;

    /**
     * Construction de l'accueil.
     *
     * @param ctrl le controleur
     */
    Accueil( Controleur ctrl )
    {
        this.ctrl = ctrl;

        this.setLayout( new BorderLayout() );

        this.pGauche = new Profil();
        this.pGauche.setPreferredSize( new Dimension( 125, 750 ) );
        this.add( this.pGauche, BorderLayout.WEST );

        this.pCentre = new MenuPrincipal();
        this.add( this.pCentre, BorderLayout.CENTER );
    }

    /**
     * Définit le pannel situé au centre de l'accueil.
     *
     * @param p le nouveau pannel qui sera placé au centre de l'accueil
     */
    private void setCentrePanel( JPanel p )
    {
        this.remove( this.pCentre );

        this.pCentre = p;
        this.add( this.pCentre, BorderLayout.CENTER );

        this.revalidate();
        this.repaint();
    }

    /**
     * @return le profil sélectionné dans la liste des profils
     */
    public String getProfilSelectionne()
    {
        return (( Profil ) this.pGauche).getProfilSelectionne();
    }

    /**
     * Classe d'affichage des profils
     */
    private class Profil extends JPanel implements ActionListener, ListSelectionListener
    {
        /**
         * Pannel scrolable pour afficher la liste des profils.
         */
        private JScrollPane listeProfils;

        /**
         * Liste des profils chargés.
         */
        private JList listeProfil;

        /**
         * Boutons pour gérer les profils.
         */
        private JButton bAjouter, bRenommer, bSupprimer;

        /**
         * Contruction du pannel pour la gestion des profils.
         */
        Profil()
        {
            this.setLayout( new BoxLayout( this, BoxLayout.PAGE_AXIS ) );

            this.listeProfil = new JList( ctrl.getListeProfil() );
            listeProfil.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
            this.listeProfil.addListSelectionListener( this );

            JPanel pImage = new JPanel();
            pImage.setLayout( new BoxLayout( pImage, BoxLayout.LINE_AXIS ) );
            pImage.add( new JLabel( new ImageIcon( "images/profil.png" ) ) );
            this.add( pImage );

            JPanel pProfils = new JPanel();
            pProfils.setLayout( new BoxLayout( pProfils, BoxLayout.LINE_AXIS ) );
            this.listeProfils = new JScrollPane( listeProfil );
            this.listeProfils.setVerticalScrollBarPolicy( JScrollPane.VERTICAL_SCROLLBAR_ALWAYS );
            this.listeProfils.setPreferredSize( new Dimension( this.getWidth(), 500 ) );
            pProfils.add( this.listeProfils );
            this.add( pProfils );

            JPanel tmp = new JPanel( new GridLayout( 3, 1, 1, 1 ) );

            this.bAjouter = new JButton( "Ajouter" );
            this.bAjouter.setLayout( new BoxLayout( bAjouter, BoxLayout.LINE_AXIS ) );
            this.bAjouter.setPreferredSize( new Dimension( this.getWidth(), 25 ) );
            this.bAjouter.addActionListener( this );
            tmp.add( this.bAjouter );

            this.bRenommer = new JButton( "Renommer" );
            this.bRenommer.setEnabled( false );
            this.bRenommer.setLayout( new BoxLayout( bRenommer, BoxLayout.LINE_AXIS ) );
            this.bRenommer.setPreferredSize( new Dimension( this.getWidth(), 25 ) );
            this.bRenommer.addActionListener( this );
            tmp.add( this.bRenommer );

            this.bSupprimer = new JButton( "Supprimer" );
            this.bSupprimer.setEnabled( false );
            this.bSupprimer.setLayout( new BoxLayout( bSupprimer, BoxLayout.LINE_AXIS ) );
            this.bSupprimer.setPreferredSize( new Dimension( this.getWidth(), 25 ) );
            this.bSupprimer.addActionListener( this );
            tmp.add( this.bSupprimer );

            this.add( tmp );
        }

        /**
         * @return le profil sélectionné dans la liste des profils
         */
        public String getProfilSelectionne()
        {
            return ( String ) this.listeProfil.getSelectedValue();
        }

        /**
         * Gestion des évènements liés aux profils.
         *
         * @param e l'évènement
         */
        @Override
        public void actionPerformed( ActionEvent e )
        {
            if ( e.getSource() == this.bAjouter )
                setCentrePanel( new EditionProfil( null ) );
            else if ( e.getSource() == this.bRenommer )
                setCentrePanel( new EditionProfil( ( String ) this.listeProfil.getSelectedValue() ) );
            else if ( e.getSource() == this.bSupprimer )
                ctrl.supprimerProfil();
        }

        /**
         * Gestion des évènements liés à la liste des profils.
         *
         * @param e l'évènement
         */
        @Override
        public void valueChanged( ListSelectionEvent e )
        {
            this.bRenommer.setEnabled( true );
            this.bSupprimer.setEnabled( true );

            if ( pCentre instanceof MenuPrincipal )
                (( MenuPrincipal ) pCentre).activerBoutons();
        }
    }

    /**
     * Classe d'affichage du menu principal.
     */
    private class MenuPrincipal extends JPanel implements ActionListener
    {
        /**
         * Les dimensions des boutons du menu principal.
         */
        private final Dimension DIMENSION_BOUTON = new Dimension( 250, 50 );

        /**
         * Boutons du menu principal.
         */
        private JButton bJouer, bNouvellePartie;

        /**
         * Contruction du menu principal.
         */
        MenuPrincipal()
        {
            this.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

            // Bouton "Jouer"
            this.bJouer = new JButton( "Jouer" );
            this.bJouer.setEnabled( getProfilSelectionne() != null );
            this.bJouer.setLayout( new BoxLayout( bJouer, BoxLayout.LINE_AXIS ) );
            this.bJouer.setPreferredSize( DIMENSION_BOUTON );
            this.bJouer.addActionListener( this );
            this.add( this.bJouer );

            // Bouton "Nouvelle Partie"
            this.bNouvellePartie = new JButton( "Nouvelle Partie" );
            this.bNouvellePartie.setEnabled( getProfilSelectionne() != null );
            this.bNouvellePartie.setLayout( new BoxLayout( bNouvellePartie, BoxLayout.LINE_AXIS ) );
            this.bNouvellePartie.setPreferredSize( DIMENSION_BOUTON );
            this.bNouvellePartie.addActionListener( this );
            this.add( this.bNouvellePartie );
        }

        /**
         * Active les boutons du menu principal.
         */
        void activerBoutons()
        {
            this.bJouer.setEnabled( true );
            this.bNouvellePartie.setEnabled( true );
        }

        /**
         * Gestion des évènements liés aux boutons du menu principal.
         *
         * @param e l'évènement
         */
        @Override
        public void actionPerformed( ActionEvent e )
        {
            if ( e.getSource() == this.bJouer )
                ctrl.jouer();
            else if ( e.getSource() == this.bNouvellePartie )
                ctrl.creerNouvellePartie();

        }
    }

    /**
     * Classe d'affichage de l'édition d'un profil.
     */
    private class EditionProfil extends JPanel implements ActionListener
    {
        /**
         * Le nom du profil.
         */
        private String nom;

        /**
         * Champ de saisie du nom du profil.
         */
        private JTextField tfNom;

        /**
         * Boutons pour valider ou annuler la création.
         */
        private JButton bValider, bAnnuler;

        /**
         * Contruction de l'édition de profil.
         */
        EditionProfil( String nom )
        {

            this.nom = nom;

            setLayout( new BoxLayout( this, BoxLayout.PAGE_AXIS ) );

            /*------------*/
            /* Formulaire */
            /*------------*/
            JPanel pForm = new JPanel();
            pForm.setLayout( new BoxLayout( pForm, BoxLayout.LINE_AXIS ) );

            this.add( new JLabel( "Nom :" ) );

            this.tfNom = new JTextField( nom );

            pForm.add( this.tfNom );
            this.add( pForm );

            /*------------*/
            /* Boutons    */
            /*------------*/
            JPanel pBoutons = new JPanel();
            pBoutons.setLayout( new BoxLayout( pBoutons, BoxLayout.LINE_AXIS ) );

            this.bValider = new JButton( "Valider" );
            this.bValider.addActionListener( this );
            pBoutons.add( this.bValider );

            this.bAnnuler = new JButton( "Annuler" );
            this.bAnnuler.addActionListener( this );
            pBoutons.add( this.bAnnuler );

            this.add( pBoutons );
        }

        /**
         * Gestion des évènements liés aux boutons.
         *
         * @param e l'évènement
         */
        @Override
        public void actionPerformed( ActionEvent e )
        {
            if ( e.getSource() == this.bValider )
            {
                if ( this.nom == null && ctrl.creerProfil( this.tfNom.getText() ) )
                    setCentrePanel( new MenuPrincipal() );
                else if ( this.nom != null && ctrl.updateProfil( this.tfNom.getText() ) )
                    setCentrePanel( new MenuPrincipal() );
            } else if ( e.getSource() == this.bAnnuler )
                setCentrePanel( new MenuPrincipal() );
        }
    }
}
