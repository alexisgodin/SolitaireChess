package fr.projet.solitaire.ihm;

import fr.projet.solitaire.Controleur;
import fr.projet.solitaire.ihm.graphic.*;
import fr.projet.solitaire.metier.Position;
import fr.projet.solitaire.metier.pieces.EPiece;
import fr.projet.solitaire.metier.pieces.Piece;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 *
 */
public class EditeurInteractifPanel extends JPanel implements MouseMotionListener, MouseListener
{
    private EditTablier  tablier;
    private Inventaire   inventaire;

    private PieceForme   selectPiece;
    private Position     selectCase;
    private Position     sourisPos;

    /**
     *
     * @param fenetre
     */
    public EditeurInteractifPanel(Fenetre fenetre)
    {
        this.setLayout(new BorderLayout());
        this.setPreferredSize(fenetre.getPreferredSize());
        this.setBackground(new Color(255, 245, 211));

        this.tablier    = new EditTablier(70);
        this.inventaire = new Inventaire(this);

        this.addMouseListener(this);
        this.addMouseMotionListener(this);

        this.repaint();
    }

    /**
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        this.tablier.dessiner(g);
        this.inventaire.dessiner(g);
        if (this.selectPiece != null)
        {
            this.selectPiece.dessiner(g);
            if (     this.selectCase != null                   &&
                    !this.inventaire.estDedans(this.sourisPos) &&
                    !this.tablier.estDedans(this.sourisPos))
            {
                ImageForme select = this.selectPiece;
                g.setColor(new Color(86, 29, 36));
                g.setFont(new Font(g.getFont().getFontName(), Font.BOLD, 16));
                g.drawString("Jeter",
                             select.getPosX() - select.getOffsetX() + select.getTailleX() / 3,
                             select.getPosY() - select.getOffsetY() + select.getTailleY());
            }
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e)
    {
        this.sourisPos = new Position(e.getX(), e.getY());

        PieceForme pieceForme = this.inventaire.getPieceForme(this.sourisPos);
        if (pieceForme == null)
        {
            pieceForme = this.tablier.getPieceForme(this.sourisPos);
            if (pieceForme != null)
                this.selectCase = this.tablier.getCase(this.sourisPos);
        }
        if (pieceForme != null)
        {
            try
            {
                this.selectPiece = new PieceForme(
                        pieceForme.getPiece(),
                        pieceForme.getPosX(),
                        pieceForme.getPosY(),
                        pieceForme.getTailleX(),
                        pieceForme.getTailleY()
                );
                this.selectPiece.zoom(PieceForme.ZOOM);
                this.selectPiece.setOffsetX(e.getX() - this.selectPiece.getPosX());
                this.selectPiece.setOffsetY(e.getY() - this.selectPiece.getPosY());
                this.selectPiece.deplacer(e.getX(), e.getY());
                this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

                this.repaint();
            }
            catch (ImageException ex)
            {
                this.selectPiece = null;
                this.selectCase  = null;
            }
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e)
    {
        this.sourisPos = new Position(e.getX(), e.getY());

        if (this.selectPiece != null)
        {
            Position cas = this.tablier.getCase(this.sourisPos);
            if (cas != null)
                this.tablier.setPiece(this.selectPiece.getPiece(), cas.getX(), cas.getY());

            if (this.selectCase != null && !this.selectCase.equals(cas))
                this.tablier.setPiece(null, this.selectCase.getX(), this.selectCase.getY());

            this.selectPiece = null;
            this.selectCase  = null;

            this.setCursor(Cursor.getDefaultCursor());
            this.repaint();
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void mouseDragged(MouseEvent e)
    {
        this.sourisPos = new Position(e.getX(), e.getY());

        if (this.selectPiece != null)
        {
            this.selectPiece.deplacer(this.sourisPos.getX(), this.sourisPos.getY());
            this.repaint();
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void mouseMoved(MouseEvent e)
    {
        this.sourisPos = new Position(e.getX(), e.getY());
    }

    // Inutile
    @Override
    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited (MouseEvent e) {}

    /* ---------------------- */
    /* TABLIER EDITABLE       */
    /* ---------------------- */

    /**
     *
     */
    private class EditTablier extends Tablier {

        /**
         * Créé la grille avec la taille donnée et son
         * nombre de cases.
         *
         * @param tailleCase
         */
        public EditTablier(int tailleCase)
        {
            super(new Piece[4][4], tailleCase);
        }

        /**
         *
         * @return
         */
        @Override
        public int getOrigineX()
        {
            return (inventaire.getTailleX() + getWidth() - getTaille()) / 2;
        }

        /**
         *
         * @return
         */
        @Override
        public int getOrigineY()
        {
            return (getHeight() - getTaille()) / 2;
        }

        /**
         *
         * @param caseX Case en X à masquer.
         * @param caseY Case en Y à masquer.
         * @return
         */
        @Override
        public boolean masquer(int caseX, int caseY)
        {
            return selectCase != null          &&
                    caseX == selectCase.getX() &&
                    caseY == selectCase.getY();
        }

        /**
         *
         * @param souris
         * @return
         */
        public boolean estDedans(Position souris)
        {
            return  getOrigineX() <= souris.getX() && souris.getX() <= getOrigineX() + getTaille() &&
                    getOrigineY() <= souris.getY() && souris.getY() <= getOrigineY() + getTaille();
        }

        /**
         *
         * @param souris
         * @return
         */
        public Position getCase(Position souris)
        {
            if (!estDedans(souris))
                return null;

            return new Position(
                    (souris.getX() - getOrigineX()) / getTailleCase(),
                    (souris.getY() - getOrigineY()) / getTailleCase()
            );
        }

        /**
         *
         * @param souris
         * @return
         */
        public PieceForme getPieceForme(Position souris)
        {
            Position cas = getCase(souris);
            if (cas == null)
                return null;

            Piece piece = getPiece(cas);
            if (piece == null)
                return null;

            try
            {
                return new PieceForme.Tablier(this, piece, cas.getX(), cas.getY());
            } catch (ImageException e)
            {
                e.printStackTrace();
                return null;
            }
        }
    }

    /* ---------------------- */
    /* INVENTAIRE              */
    /* ---------------------- */

    /**
     *
     */
    private class Inventaire implements Dessinable {

        private PieceItem[] pieceItems;
        private JPanel      conteneur;

        /**
         *
         * @param conteneur
         */
        public Inventaire(JPanel conteneur)
        {
            this.conteneur  = conteneur;
            this.pieceItems = new PieceItem[EPiece.values().length];

            int pos = 0;
            for (EPiece ePiece : EPiece.values())
            {
                try
                {
                    this.pieceItems[pos++] = new PieceItem(this, ePiece.creerPiece());
                } catch (ImageException e)
                {
                    e.printStackTrace();
                }
            }
        }

        // GETTERS
        private int getOrigineX() { return 0;                                 }
        private int getOrigineY() { return 0;                                 }
        private int getTailleX()  { return this.conteneur.getWidth() / 8 + 1; }
        private int getTailleY()  { return this.conteneur.getHeight();        }
        private int getNbCase()   { return this.pieceItems.length;            }

        /**
         *
         * @param piece
         * @return
         */
        private int getOrigineYItem(Piece piece)
        {
            int pos = 0;
            while (!this.pieceItems[pos].getPieceForme().getPiece().equals(piece))
                pos++;

            return pos * (getTailleY() / getNbCase());
        }

        /**
         *
         * @param souris
         * @return
         */
        public boolean estDedans(Position souris)
        {
            return  getOrigineX() <= souris.getX() && souris.getX() <= getOrigineX() + getTailleX() &&
                    getOrigineY() <= souris.getY() && souris.getY() <= getOrigineY() + getTailleY();
        }

        /**
         *
         * @param position
         * @return
         */
        private PieceForme getPieceForme(Position position)
        {
            if (!estDedans(position))
                return null;

            int pos = (getOrigineY() + position.getY()) / (getTailleY() / getNbCase());
            if (pos >= 0 && pos < pieceItems.length)
                return pieceItems[pos].getPieceForme();

            return null;
        }

        /**
         *
         * @param g Objet de dessin graphique.
         */
        @Override
        public void dessiner(Graphics g)
        {
            g.setColor(new Color(197, 182, 160));
            g.fillRect(getOrigineX(), getOrigineY(), getTailleX() , getTailleY());

            for (PieceItem pieceItem : pieceItems)
                pieceItem.dessiner(g);
        }
    }

    /* ---------------------- */
    /* ITEM              */
    /* ---------------------- */

    /**
     *
     */
    private class PieceItem implements Dessinable
    {
        private int PIECE_TAILLE_MAX = 60;

        private Inventaire inventaire;
        private PieceForme pieceForme;

        /**
         *
         * @param inv
         * @param piece
         * @throws ImageException
         */
        PieceItem(final Inventaire inv, Piece piece) throws ImageException {
            this.inventaire = inv;
            this.pieceForme = new PieceForme(piece)
            {
                @Override
                public  int getPosX()    { return inv.getOrigineX() + (inv.getTailleX() - getTailleX()) / 2; }
                public  int getPosY()    { return inv.getOrigineYItem(this.getPiece()) + (PieceItem.this.getTailleY() - getTailleY()) / 2; }
                public  int getTailleX() { return getTaille();                      }
                public  int getTailleY() { return getTaille();                      }
                private int getTaille()
                {
                    int taille = inv.getTailleX() < (inv.getTailleY() / inv.getNbCase()) ?
                            inv.getTailleX() : (inv.getTailleY() / inv.getNbCase());
                    return taille > PIECE_TAILLE_MAX ? PIECE_TAILLE_MAX : taille;
                }
            };
            this.pieceForme.zoom(0.9F);
        }

        // GETTER
        private int       getOrigineX()   { return inventaire.getOrigineX();                            }
        private int       getOrigineY()   { return inventaire.getOrigineYItem(pieceForme.getPiece());   }
        private int       getTailleX()    { return inventaire.getTailleX();                             }
        private int       getTailleY()    { return inventaire.getTailleY() / inventaire.getNbCase() - 1; }
        public PieceForme getPieceForme() { return this.pieceForme;                                    }

        /**
         *
         * @param g Objet de dessin graphique.
         */
        @Override
        public void dessiner(Graphics g)
        {
            if (this.pieceForme.equals(EditeurInteractifPanel.this.selectPiece))
            {
                g.setColor(new Color(0, 0, 0, 71));
                g.fillRect(
                        getOrigineX() - 5,
                        getOrigineY(),
                        getTailleX() + 5,
                        getTailleY()
                );
            }

            g.setColor(new Color(137, 128, 109));
            g.drawRect(
                    getOrigineX(),
                    getOrigineY(),
                    getTailleX() ,
                    getTailleY()
            );

            this.pieceForme.dessiner(g);
        }
    }
}
