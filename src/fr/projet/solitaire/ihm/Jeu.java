package fr.projet.solitaire.ihm;

import fr.projet.solitaire.ihm.menus.BarreMenus;
import fr.projet.solitaire.metier.Defi;
import fr.projet.solitaire.metier.pieces.Piece;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe d'affichage d'une partie de Solitaire Chess.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class Jeu extends JPanel implements ActionListener
{
    /**
     * La fenêtre principale.
     */
    private Fenetre fenetre;

    /**
     * Le plateau du jeu.
     */
    private Plateau plateau;

    /**
     * Le cimetière du jeu.
     */
    private Cimetiere cimetiere;

    /**
     * Les boutons du jeu.
     */
    private JButton bPrecedent, bSuivant, bDefi, bAnnulerAction;

    /**
     * Les informations liés au jeu.
     */
    private JLabel lDefi, lNDefi, lCoups, nbCoups, nbCoupMinimum;

    /**
     * Construction du jeu.
     *
     * @param fenetre la fenêtre principal
     */
    Jeu( Fenetre fenetre )
    {
        this.fenetre = fenetre;

        this.setSize( this.fenetre.getWidth(), this.fenetre.getHeight() );

        this.plateau = new Plateau( fenetre );
        this.cimetiere = new Cimetiere();

        this.setLayout( new BorderLayout() );

        JPanel pUtil = new JPanel( new BorderLayout() );

        JPanel pOutils = new JPanel( new GridLayout( 1, 3 ) );
        this.bPrecedent = new JButton();
        this.bPrecedent.setIcon( new ImageIcon( "images/fleche-gauche.png" ) );
        this.bPrecedent.setToolTipText( "Défis précédent" );
        this.bPrecedent.addActionListener( new PrecedentListener( fenetre.getControleur() ) );
        if ( this.fenetre.getControleur().getIndiceDefiCourant() == 0 )
            this.bPrecedent.setEnabled( false );
        pOutils.add( bPrecedent );

        this.bDefi = new JButton();
        this.bDefi.setIcon( new ImageIcon( "images/bouton_recommencer.png" ) );
        this.bDefi.setToolTipText( "Réinitialiser le défi courant" );
        this.bDefi.addActionListener( this );
        pOutils.add( bDefi );

        this.bAnnulerAction = new JButton();
        this.bAnnulerAction.setIcon( new ImageIcon( "images/bouton_annuler.png" ) );
        this.bAnnulerAction.setToolTipText( "Annuler la dernière action" );
        this.bAnnulerAction.addActionListener( this );
        pOutils.add( bAnnulerAction );

        this.bSuivant = new JButton();
        this.bSuivant.setIcon( new ImageIcon( "images/fleche-droite.png" ) );
        this.bSuivant.setToolTipText( "Défis suivant" );
        this.bSuivant.addActionListener( this );
        if ( !this.fenetre.getControleur().getManager().aUnDefiSuivantDbloque() )
            this.bSuivant.setEnabled( false );
        pOutils.add( bSuivant );

        pUtil.add( pOutils, BorderLayout.NORTH );

        JPanel pCoups = new JPanel();
        this.lDefi = new JLabel( "Defi actuel : " );
        pCoups.add( lDefi );
        this.lNDefi = new JLabel( fenetre.getControleur().getNomDefi() );
        pCoups.add( lNDefi );

        this.lCoups = new JLabel( "Nombre de coups au total : " );
        this.nbCoups = new JLabel( "" + Defi.getNbCoups() );
        this.nbCoupMinimum = new JLabel( "/" + this.fenetre.getControleur().getNbCoupMinimum() + " mini" );
        pCoups.add( this.lCoups );
        pCoups.add( this.nbCoups );
        pCoups.add( this.nbCoupMinimum );
        pUtil.add( pCoups, BorderLayout.SOUTH );
        this.add( pUtil, BorderLayout.NORTH );

        this.add( plateau );

        this.cimetiere.setPreferredSize( new Dimension( this.getWidth(), this.getHeight() / 6 ) );
        this.add( this.cimetiere, BorderLayout.SOUTH );

        this.plateau.repaint();
        this.cimetiere.repaint();
    }

    /**
     * Met à jour l'affichage du jeu.
     */
    public void update()
    {
        this.plateau.setPieces( this.fenetre.getControleur().getPieces() );
        this.lNDefi.setText( this.fenetre.getControleur().getNomDefi() );
        this.nbCoups.setText( "" + Defi.getNbCoups() );
        this.nbCoupMinimum.setText( "/" + this.fenetre.getControleur().getNbCoupMinimum() + " mini" );

        if ( this.fenetre.getControleur().getIndiceDefiCourant() == 0 )
            this.bPrecedent.setEnabled( false );
        else
            this.bPrecedent.setEnabled( true );

        if ( !this.fenetre.getControleur().getManager().aUnDefiSuivantDbloque() )
            this.bSuivant.setEnabled( false );
        else
            this.bSuivant.setEnabled( true );

        this.cimetiere.update();
    }

    /**
     * Debloque un défi.
     *
     * @param niveau         le niveau du défi
     * @param defiADebloquer le défi à débloquer
     */
    public void debloquerDefi( Defi.Niveau niveau, Defi defiADebloquer )
    {
        this.cimetiere.reinitialiser();
        (( BarreMenus ) this.fenetre.getJMenuBar()).getMenuListeNiveaux().debloquerDefi( niveau, defiADebloquer );
    }

    /**
     * Détruit la pièce passée en paramètre du plateau et
     * l'ajoute au cimetière.
     *
     * @param piece la pièce a détruire
     */
    public void tuerPiece( Piece piece )
    {
        this.cimetiere.ajouterTue( piece );
        this.cimetiere.update();
    }

    /**
     * Réinitialise le cimetière.
     */
    public void reinitialiserCimetiere()
    {
        this.cimetiere.reinitialiser();
    }

    /**
     * Gestion des évènements liés aux boutons du jeu.
     *
     * @param e l'évènement
     */
    @Override
    public void actionPerformed( ActionEvent e )
    {
        if ( e.getSource() == this.bSuivant )
        {
            this.fenetre.getControleur().defiSuivant();
        }

        if ( e.getSource() == this.bDefi )
        {
            if ( this.fenetre.getControleur().getDefiCourant().getNbCoupsDefi() != 0 )
                Defi.setNbCoups( +2 );

            this.fenetre.getControleur().setDefiCourant( this.fenetre.getControleur().getListeDefis( null ).indexOf( this.fenetre.getControleur().getDefiCourant() ) );
        }

        if ( e.getSource() == this.bAnnulerAction )
        {
            this.fenetre.getControleur().annulerAction();
        }
    }

    /**
     * @return le cimetière du jeu
     */
    public Cimetiere getCimetiere()
    {
        return this.cimetiere;
    }

    /**
     * @return le plateau du jeu
     */
    public Plateau getPlateau()
    {
        return this.plateau;
    }
}
