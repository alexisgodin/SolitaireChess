package fr.projet.solitaire.ihm;

import fr.projet.solitaire.Controleur;

import javax.swing.*;
import java.awt.*;

/**
 *
 */
public class EditeurPanel extends JPanel
{
    private Fenetre                fenetre;
    private EditeurInteractifPanel editor;

    public EditeurPanel(Fenetre fenetre)
    {
        this.setLayout(new BorderLayout());

        this.fenetre = fenetre;
        this.editor  = new EditeurInteractifPanel(fenetre);

        JLabel titre = new JLabel("Editeur", JLabel.CENTER);
        titre.setPreferredSize(new Dimension(getWidth(), 20));

        JPanel actionPanel = new JPanel();
        actionPanel.add(new JButton("Retour"));
        actionPanel.add(new JButton("Sauvegarder"));
        actionPanel.add(new JLabel("Nom :"));
        actionPanel.add(new JTextField(15));
        actionPanel.setPreferredSize(new Dimension(getWidth(), 30));


        this.add(this.editor, BorderLayout.CENTER);
        //this.add(titre, BorderLayout.PAGE_START);
        this.add(actionPanel, BorderLayout.NORTH);
    }


    /*public static void main(String[] args) {
        Controleur ctrl = new Controleur();

        JFrame frame = new JFrame("Editeur TEST");
        frame.getContentPane().add(new EditeurPanel(ctrl.getFenetre()));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }*/
}
