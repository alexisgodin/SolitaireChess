package fr.projet.solitaire.ihm.menus;

import fr.projet.solitaire.ihm.Fenetre;

import javax.management.JMException;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.net.URI;

/**
 * Barre des menus de la fenêtre.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */

public class BarreMenus extends JMenuBar implements ActionListener
{
    /**
     * Fenetre du logiciel.
     */
    private Fenetre fenetre;

    /**
     * Sous menu pour désactiver l'aide.
     */
    JMenuItem desactivationAide;

    /**
     * Menu "Aide".
     */
    private JMenu menuAide;
	/**
     * Menu "Fichier".
     */
    private MenuFichier menuFichier;

    /**
     * Création de la barre des menus.
     */
    public BarreMenus(Fenetre fenetre)
    {
        this.fenetre = fenetre;

        this.menuFichier = new MenuFichier(fenetre);
        this.add( menuFichier );

        // Defis
        MenuListeDefis defis = new MenuListeDefis(fenetre);
        defis.setEnabled( false );
        this.add( defis);

        // Aide
        this.menuAide = new JMenu( "Aide" );
        JMenuItem regles = new JMenuItem("Règles");
        regles.setIcon( new ImageIcon( "images/aide.png" ) );
        regles.addActionListener(this);
        regles.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK));

        this.desactivationAide = new JMenuItem("Désactiver aide");
        this.desactivationAide.addActionListener(this);
        this.desactivationAide.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));

        this.menuAide.add(regles);
        this.menuAide.add(desactivationAide);
        this.add(this.menuAide);
    }


	/**
     * Accesseur du MenuListeDefis.
     * @return le menu de listes de défis.
     */
    public MenuListeDefis getMenuListeNiveaux()
    {
        return (MenuListeDefis) this.getMenu(1);
    }

	/**
     * Accesseur du MenuFichier.
     * @return le menu fichier
     */
    public MenuFichier getMenuFichier() { return this.menuFichier; }

	/**
     * Modificateur pour gérer l'activation et la non activation du Menu Defi.
     * En effet, si on est dans l'accueil, le menu de défis est grisé et non utilisable.
     * @param b true si on veut rendre le menu cliquable, false sinon
     */
    public void activationMenuDefi(boolean b)
    {
        this.getMenu( 1 ).setEnabled( b );
    }

	/**
     * Méthode de gestion des évènements liés aux JMenuItem.
     * @param e un évènement
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        {
            if ( e.getSource() instanceof JMenuItem                    &&
                 ((JMenuItem)e.getSource()).getText().equals("Règles")    )
            {
                try
                {
                    File fichier = new File("regles.html");
                    URI uri = URI.create(fichier.getName());

                    if (System.getProperties().get("os.name").toString().startsWith("Windows"))
                        Runtime.getRuntime().exec("cmd /c start " + uri.getPath());

                    if (System.getProperties().get("os.name").toString().startsWith("Linux"))
                        Runtime.getRuntime().exec("/etc/alternatives/x-www-browser " + uri.getPath());
                } catch (Exception ignored) {}
            }

            if(e.getSource() == this.desactivationAide && this.desactivationAide.getText().equals("Désactiver aide"))
            {
                this.fenetre.getControleur().setAideAutorise(false);
                ((JMenuItem)e.getSource()).setText("Activer aide");
                this.fenetre.getJeu().update();
            }
            else if(e.getSource() == this.desactivationAide && this.desactivationAide.getText().equals("Activer aide"))
            {
                this.fenetre.getControleur().setAideAutorise(true);
                ((JMenuItem)e.getSource()).setText("Désactiver aide");
                this.fenetre.getJeu().update();
            }
        }
    }
}
