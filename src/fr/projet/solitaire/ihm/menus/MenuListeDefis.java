package fr.projet.solitaire.ihm.menus;

import fr.projet.solitaire.Controleur;
import fr.projet.solitaire.ihm.Fenetre;
import fr.projet.solitaire.ihm.PrecedentListener;
import fr.projet.solitaire.metier.Defi;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Classe de création de l'onglet "Niveau" de la barre des menus.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */
public class MenuListeDefis extends JMenu implements ActionListener
{
    Controleur ctrl;
    private JMenuItem precedent, suivant, annuler;

    /**
     * Construction de l'onglet "Niveau"
     */
    MenuListeDefis(Fenetre fenetre )
    {
        super( "Defis" );

        this.ctrl = fenetre.getControleur();

        // Création des menus de niveaux
        this.add( new NiveauMenu( Defi.Niveau.DEBUTANT      , ctrl ) );
        this.add( new NiveauMenu( Defi.Niveau.INTERMEDIAIRE , ctrl ) );
        this.add( new NiveauMenu( Defi.Niveau.AVANCE        , ctrl ) );
        this.add( new NiveauMenu( Defi.Niveau.EXPERT        , ctrl ) );

        this.precedent = new JMenuItem("Précédent");
        this.precedent.addActionListener(new PrecedentListener(ctrl));
        this.precedent.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
        this.suivant = new JMenuItem("Suivant");
        this.suivant.addActionListener(this);
        this.suivant.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));

        this.addSeparator();
        this.add(this.precedent);
        this.add(this.suivant);
    }

    /**
     * Débloque le défi passer en paramètre et de niveau donné.
     * @param niveau
     * @param defiADebloque
     */
    public void debloquerDefi( Defi.Niveau niveau, Defi defiADebloque )
    {
        if(niveau == null)
            return;
        NiveauMenu tmp = this.getMenuCorrespondantA(niveau);

        if (tmp != null)
            tmp.debloquerDefi(defiADebloque);
    }

    /**
     * Retourne le NiveauMenu correspondant au Niveau passé en paramètre.
     * @param n
     * @return
     */
    public NiveauMenu getMenuCorrespondantA(Defi.Niveau n)
    {
        for(int i = 0; i < this.getItemCount(); i ++)
        {
            if( this.getItem(i) instanceof NiveauMenu  &&
                ((NiveauMenu) this.getItem(i)).getNiveau() == n)
                return (NiveauMenu) this.getItem(i);
        }

        return null;
    }

    /**
     * Méthode gérant l'évenementielle.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this.suivant)
        {
            ctrl.defiSuivant();
        }
    }

    /**
     * Classe de gestion des menus niveaux
     */
    private class NiveauMenu extends JMenu implements ActionListener
    {
        private int i = 0;

        private Defi.Niveau niveau;

        /**
         * Construction du menu du niveau correpondant à sa
         * difficulté et création des défis.
         *
         * @param niveau le niveau correspondant
         */
        NiveauMenu( Defi.Niveau niveau, Controleur ctrl )
        {
            super( niveau.name() );

            this.niveau = niveau;

            for (Defi defi : ctrl.getListeDefis(niveau) )
            {
                DefiMenuItem tmp =  new DefiMenuItem(defi);
                tmp.addActionListener(this);

                this.add(tmp);
                tmp.setEnabled( defi.estDebloque() );
            }

        }

		/**
         * Accesseur pour le niveau d'un défi.
         * @return le niveau d'un défi
         */
        public Defi.Niveau getNiveau() {
            return niveau;
        }

        /**
         * Débloque le defi passé en paramètre.
         * @param defiADebloquer
         */
        public void debloquerDefi( Defi defiADebloquer )
        {
            try
            {
                this.getItem(this.getIndiceDefi(defiADebloquer)).setEnabled( defiADebloquer.estDebloque());
            } catch ( Exception ignored ) {}
        }

		/**
         * Accesseur pour obtenir l'indice d'un défi.
         * @param defi un défi
         * @return l'indice du défi
         */
        private int getIndiceDefi(Defi defi)
        {
            for(int i = 0; i < this.getItemCount(); i++)
                if (this.getItem(i) instanceof DefiMenuItem &&
                        ((DefiMenuItem) this.getItem(i)).getDefi().getNom().equals(defi.getNom()))
                {
                    return i;
                }
            return -1;
        }


        /**
         * Gestion des évènements.
         *
         * @param e l'évènement
         */
        @Override
        public void actionPerformed( ActionEvent e )
        {
            for ( int i = 0; i < this.getItemCount(); i++ )
            {
                if ( e.getSource() == this.getItem( i ) )
                {
                    /**
                     * Cette conditionnelle empeche le joueur d'aller au niveau precedent
                     * si il a déja déplacé une pièce sur le defi courant (empeche la triche).
                     */
                    if (ctrl.getDefiCourant().getNbCoupsDefi() == 0)
                        ctrl.setDefiCourant(this.getIndiceDefiFromManager(((DefiMenuItem)this.getItem(i)).getDefi()));
                }
            }
        }

        /**
         * Retourne l'indice dans le manager du défi passé en paramètre.
         * @param d
         * @return
         */
        public int getIndiceDefiFromManager(Defi d)
        {
            int cpt = 0;
            for(Defi defi : ctrl.getListeDefis(null))
            {
                if (defi.getNom().equals(d.getNom()))
                    return cpt;
                cpt ++;
            }

            return -1;
        }

        /**
         * Classe qui gère les sous-menus constitué de défi.
         */
        public class DefiMenuItem extends JMenuItem
        {
            /**
             * Défi du MenuItem
             */
            private Defi defi;

            /**
             * Constructeur.
             * @param defi
             */
            public DefiMenuItem(Defi defi)
            {
                super(defi.getNom());
                this.defi = defi;
            }

            /**
             * Getter.
             * @return le défi
             */
            public Defi getDefi() { return this.defi; }
        }
    }
}
