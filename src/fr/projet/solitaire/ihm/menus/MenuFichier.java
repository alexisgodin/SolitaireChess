package fr.projet.solitaire.ihm.menus;

import fr.projet.solitaire.ihm.Fenetre;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Classe de gestion de l'onglet "Fichier" de la barre des menus.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */

public class MenuFichier extends JMenu implements ActionListener
{
    private Fenetre fenetre;

    /**
     * Construction de l'onglet "Fichier".
     */
    MenuFichier( Fenetre fenetre )
    {
        super( "Fichier" );

        this.fenetre = fenetre;

        // Accueil (indice: 0)
        JMenuItem accueil = new JMenuItem( "Accueil" );
        accueil.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
        accueil.addActionListener( this );

        accueil.setIcon( new ImageIcon( "images/maison.png" ) );
        this.add( accueil );

        // Annuler action (1)
        JMenuItem annuler = new JMenuItem( "Annuler action" );
        annuler.addActionListener( this );
        annuler.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK) );
        this.add( annuler );

        this.addSeparator();    // 2

        // Quitter (indice: 3)
        JMenuItem quitter = new JMenuItem( "Quitter" );
        quitter.setIcon( new ImageIcon( "images/shutdown.png" ) );
        quitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
        quitter.addActionListener( this );
        this.add( quitter );
    }

    /**
     * Modificateur pour gérer l'activation et la non activation du JMenuItem.
     * En effet, si on est dans l'accueil, le JMenuItem accueil est grisé et non utilisable.
     * @param b true si on veut rendre le menu cliquable, false sinon
     */
    public void activationMenuFichier(boolean b) {
        this.getItem(0).setEnabled(b);
    }

    /**
     * Gestion des évènements liés à l'onglet "Fichier".
     *
     * @param e l'évènement
     */
    @Override
    public void actionPerformed( ActionEvent e )
    {
        if (e.getSource() == this.getItem( 0 )) {
            JOptionPane.showOptionDialog(this.fenetre, "Vous allez retourner à l'accueil." + "\nVotre progression est sauvegardée depuis le dernier défi terminé.", "Retour accueil", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
            this.fenetre.afficherAccueil();
        }

        else if ( e.getSource() == this.getItem( 1 ) )
            this.fenetre.getControleur().annulerAction();
        else if ( e.getSource() == this.getItem( 3 ) )
        {
            JOptionPane.showMessageDialog(this.fenetre, "Merci d'avoir joué ! A bientôt ! :-(", "A bientôt", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
    }
}
