package fr.projet.solitaire.ihm;

import fr.projet.solitaire.Controleur;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe de gestion des actions du bouton et du JMenuItem "Defi Precedent".
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */

public class PrecedentListener implements ActionListener
{
	/**
	 * Controleur
	 */
	private Controleur  ctrl;

	/**
	 * Objets qui servent à faire le message d'avertissement
	 */
	private JDialog     dialog;
	private JOptionPane optionsPane;

	/**
	 * Les boutons valider et annuler
	 */
	private JButton     bConfirmer, bAnnuler;

	/**
	 * Constructeur
	 * @param ctrl
     */
	public PrecedentListener(Controleur ctrl)
	{
		this.ctrl = ctrl;

		this.bConfirmer = new JButton("Confirmer");
		this.bConfirmer.addActionListener(this);
		this.bAnnuler = new JButton("Annuler");
		this.bAnnuler.addActionListener(this);

		this.optionsPane = new JOptionPane();
		this.optionsPane.setMessage("Si vous revenez au défi précedent puis que vous retournez immédiatement" +
				"\n" + "sur le défi, cela entraînera une pénalité de 2 déplacements.");
		this.optionsPane.setMessageType(JOptionPane.WARNING_MESSAGE);
		Object[] choix = { bConfirmer, bAnnuler };
		this.optionsPane.setOptions(choix);
		this.dialog = optionsPane.createDialog(null, "Attention");
	}

	/**
	 * Méthode qui gère l'évenementielle
	 * @param actionEvent
     */
	@Override
	public void actionPerformed(ActionEvent actionEvent)
	{
		if (ctrl.getListeDefis(null).indexOf(ctrl.getDefiCourant()) != 0)
		{
			if ( ctrl.getDefiCourant().getNbCoupsDefi() != 0 &&
					!ctrl.aUnDefiSuivantDebloque()                  )
			{
				this.dialog.setVisible(true);
			}
			else
				ctrl.defiPrecedent();
		}

		if (actionEvent.getSource() == this.bConfirmer)
		{
			ctrl.setAvertissement(ctrl.getIndiceDefiCourant());
			ctrl.defiPrecedent();
			this.dialog.dispose();
		}

		if (actionEvent.getSource() == this.bAnnuler)
		{
			this.dialog.dispose();
		}
	}
}
