package fr.projet.solitaire.ihm;

import fr.projet.solitaire.Controleur;
import fr.projet.solitaire.ihm.graphic.AideForme;
import fr.projet.solitaire.ihm.graphic.ImageException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe de gestion du système d'aide pour l'utilisateur.
 *
 * @author Alexis GODIN
 *         Erik KUBIAK
 *         Quentin MARCOTTE
 *         Yoann EICHELBERGER
 */

public class GestionnaireAide
{
    /**
     * Durée avant que l'aide apparaisse
     */
    private static final int TEMPS_TIMER_AIDE = 10*1000;

    /**
     * Controleur et plateau utiliser pour la gestion des aides
     */
    private Controleur ctrl;
    private Plateau plateau;

    /**
     * Timer utilisé pour faire apparaître l'image d'aide
     */
    private Timer timer;

    /**
     * Image d'ampoule
     */
    private AideForme ampoule;


    /**
     * Constructeur
     *
     * @param ctrl
     * @param plateau
     */
    public GestionnaireAide(Controleur ctrl, Plateau plateau)
    {
        this.ctrl    = ctrl;
        this.plateau = plateau;

        this.initTimer();

        try
        {
            this.ampoule = new AideForme(0, 0, 0, 0);
        } catch (ImageException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui initialise le timer
     */
    private void initTimer()
    {
        if(this.ctrl.getAideAutorise())
        {
            this.timer = new Timer(GestionnaireAide.TEMPS_TIMER_AIDE, new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    plateau.update();
                    timer.stop();
                }
            });

            this.timer.setRepeats(false);
            this.timer.start();
        }
    }

    /**
     * Méthode qui dessine l'image si une aide existe pour le niveau et sqi le timer est fini
     * @param g objet Graphics qui sert à dessiner l'image
     * @param x coordonnée
     * @param y coordonnée
     * @param taille de l'image
     */
    public void dessiner(Graphics g, int x, int y, int taille )
    {
        if(this.ctrl.getDefiCourant().defiPossedeAide() && !this.timer.isRunning() &&
           this.ctrl.getAideAutorise()                                                 )
        {
            this.ampoule.setTailleX(taille);
            this.ampoule.setTailleY(taille);
            this.ampoule.deplacer(x, y);
            this.ampoule.dessiner(g);
        }
    }

    /**
     * @return si le timer est fini
     */
    public boolean timerEstFini()
    {
        return !this.timer.isRunning();
    }

    /**
     * Relance le timer
     */
    public void restartTimer()
    {
        this.timer.restart();
    }

    /**
     * Effectue l'aide si on clique sur l'image
     * @param x coordonnée
     * @param y coordonnée
     * @param taille de l'image
     * @return si l'aide à été effectuée
     */
    public boolean help(int x, int y, int taille)
    {
        if(this.timerEstFini() && this.positionSurImage(x, y, taille))
        {
            this.ctrl.aider();
            return true;
        }

        return false;
    }

    /**
     * @param x coordonnée
     * @param y coordonnée
     * @param taille de l'image
     * @return si la position est sur l'image
     */
    private boolean positionSurImage(int x, int y, int taille)
    {
        return x >= this.ampoule.getPosX() && x < this.ampoule.getPosX() + taille &&
               y >= this.ampoule.getPosY() && y < this.ampoule.getPosY() + taille    ;
    }
}
